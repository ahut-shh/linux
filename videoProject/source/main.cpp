#include"util.hpp"
#include"data.hpp"
#include"server.hpp"
#include<iostream>

// //////////////////////////////////////////
// //File
// //////////////////////////////////////////
// void test01()
// {
//     std::string s="<html></html>";
//     aod::FileUtil("./www").CreateDirector();
//     aod::FileUtil("./www/index.html").SetContent(s);
//     std::string body;
//     aod::FileUtil("./www/index.html").GetContent(&body);
//     std::cout<<body<<std::endl;
// }
// //////////////////////////////////////////
// //Json
// //////////////////////////////////////////
// void test02()
// {
//     json j;
//     j["姓名"]="小张";
//     j["年龄"]=18;
//     j["性别"]="男";
//     j["成绩"].push_back(77.5);
//     j["成绩"].push_back(60);
//     j["成绩"].push_back(79);

//     std::string body;
//     aod::JsonUtil::Serialize(j,&body);
//     std::cout<<body<<std::endl;

//     json val;
//     aod::JsonUtil::UnSerialize(body,&val);
//     std::cout<<val["姓名"]<<std::endl;

// }
//////////////////////////////////////////
//Data
//////////////////////////////////////////
// void test03()
// {
//     aod::TableVideo tb_video;
//     json video;
//     // video["name"]="变形金刚";
//     // video["info"]="这是会变形的玩具";
//     // video["video"]="/video/robot.map";
//     // video["image"]="/image/robot.jpg";

//     // tb_video.Insert(video);
//     // tb_video.Update(2,video);
//     tb_video.Delete(3);

// }
//////////////////////////////////////////
//httplib
//////////////////////////////////////////
void test04()
{
    aod::Server server(9090);
    server.RunModule();
}


int main()
{
    test04();
    return 0;
}