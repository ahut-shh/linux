#include "./httplib.h"

using namespace httplib;

void HelloBit(const Request &req, Response &rsp) {
    rsp.set_content("hello world", "text/plain");
    // rsp.status = 200; // 可以忽略 ，默认httplib会加上一个200的状态码
}

void Numbers(const Request& req, Response &rsp) {
    std::string num = req.matches[1];
    rsp.set_content(num, "text/plain");
}

void Multipart(const Request& req, Response &rsp) {
    MultipartFormData file = req.get_file_value("file1");
    std::cout << file.filename << std::endl;
    std::cout << file.content << std::endl;
}

int main() {
    Server server;

    // 设置一个静态资源根目录
    server.set_mount_point("/", "./www");

    // 添加请求-处理映射信息
    server.Get("/hi", HelloBit);
    server.Get(R"(/numbers/(\d+))", Numbers);
    server.Post("/multipart", Multipart);

    if (server.listen("0.0.0.0", 9090)) {
        std::cout << "Server started on port 9090" << std::endl;
    } else {
        std::cerr << "Failed to start server on port 9090" << std::endl;
    }

    return 0;
}