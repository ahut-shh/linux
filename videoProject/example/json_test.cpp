#include <iostream>
#include <string>
#include <nlohmann/json.hpp>

// 为方便使用，使用命名空间
using json = nlohmann::json;

int main() {
    const char *name = "小明";
    int age = 19;
    float score[] = {77.5, 88, 99.5};

    json val;
    val["姓名"] = name;
    val["年龄"] = age;
    val["成绩"] = {score[0], score[1], score[2]};

    // 输出格式化后的 JSON 字符串
    std::cout << val.dump() << std::endl;

    json j=json::parse(val.dump());
    std::cout<<j<<std::endl;

    return 0;
}