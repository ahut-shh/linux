#include "../mqserver/mq_route.hpp"
#include <gtest/gtest.h>
#include <vector>

class RouteTest:public testing::Environment{
    public:
        virtual void SetUp() override
        {

        }
        virtual void TearDown() override
        {

        }
};
TEST(route_test,legal_routing_key)
{
    std::string rkey1="newq._music.pop";
    std::string rkey2="newq..music.pop";
    std::string rkey3="newq.,music.pop";
    std::string rkey4="newq.123_music.pop";
    ASSERT_EQ(mq::Router::isLegalRoutingKey(rkey1),true);
    ASSERT_EQ(mq::Router::isLegalRoutingKey(rkey2),true);
    ASSERT_EQ(mq::Router::isLegalRoutingKey(rkey3),false);
    ASSERT_EQ(mq::Router::isLegalRoutingKey(rkey4),true);

}
TEST(route_test,legal_binding_key)
{
    std::string bkey1="newq._music.pop";
    std::string bkey2="newq.#._music.pop";
    std::string bkey3="newq.*._music.pop";
    std::string bkey4="newq.*.#._music.pop";
    ASSERT_EQ(mq::Router::isLegalBindingKey(bkey1), true);
    ASSERT_EQ(mq::Router::isLegalBindingKey(bkey2), true);
    ASSERT_EQ(mq::Router::isLegalBindingKey(bkey3), true);
    ASSERT_EQ(mq::Router::isLegalBindingKey(bkey4), false);
    
}
TEST(route_test,route)
{
    std::vector<std::string> bkeys={
        "aaa",
        "aaa.bbb",
        "aaa.bbb.#",
        "aaa.bbb.*"
    };
    std::vector<std::string> rkeys={
        "aaa",
        "aaa.bbb",
        "aaa.bbb.pop",
        "aaa.bbb.music"
    };
    std::vector<bool> result={
        true,
        true,
        true,
        true
    };
    for(int i=0;i<bkeys.size();i++)
    {
        ASSERT_EQ(mq::Router::route(mq::ExchangeType::TOPIC, rkeys[i], bkeys[i]), result[i]);
    }
}

int main(int argc,char* argv[])
{
    testing::InitGoogleTest(&argc,argv);
    testing::AddGlobalTestEnvironment(new RouteTest);
    RUN_ALL_TESTS();
    return 0;
}