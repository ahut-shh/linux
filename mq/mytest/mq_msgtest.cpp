#include "../mqserver/mq_message.hpp"
#include <gtest/gtest.h>

mq::MessageManager::ptr mmp;

class MessageTest:public testing::Environment{
    public:
        virtual void SetUp() override
        {
            mmp=std::make_shared<mq::MessageManager>("./data/message");
            mmp->initQueueMessage("queue1");
        }
        virtual void TearDown() override
        {
            // mmp->clear();
            std::cout<<"最后的清理"<<std::endl;
        }
};

//新增消息测试：新增消息，然后观察可获取消息数量，以及持久化消息数量
TEST(message_test,insert_test)
{
    mq::BasicProperties properties;
    properties.set_id(mq::UuidHelpr::uuid());
    properties.set_delivery_mode(mq::DeliveryMode::DURABLE);
    properties.set_routing_key("news.music.pop");
    mmp->insert("queue1",&properties,"hello world-1",mq::DeliveryMode::DURABLE);
    mmp->insert("queue1",nullptr,"hello world-2",mq::DeliveryMode::DURABLE);
    mmp->insert("queue1",nullptr,"hello world-3",mq::DeliveryMode::DURABLE);
    mmp->insert("queue1",nullptr,"hello world-4",mq::DeliveryMode::DURABLE);
    mmp->insert("queue1",nullptr,"hello world-5",mq::DeliveryMode::UNKNOWMODE);
    ASSERT_EQ(mmp->total_count("queue1"),4);
    ASSERT_EQ(mmp->durable_count("queue1"),4);
    ASSERT_EQ(mmp->push_count("queue1"),5);
    ASSERT_EQ(mmp->waitack_count("queue1"),0);
}
//获取消息测试：获取一条消息，然后在不进行确认，以及进行确认后，查看可获取消息数量，待确认消息数量，以及测试消息获取的顺序
// TEST(message_test,select_test)
// {
//     mq::MessagePtr msg1=mmp->front("queue1");
//     ASSERT_EQ(msg1->payload().body(),std::string("hello world-1"));
//     ASSERT_EQ(mmp->push_count("queue1"),4);
//     ASSERT_EQ(mmp->waitack_count("queue1"),1);

//     mq::MessagePtr msg2=mmp->front("queue1");
//     ASSERT_EQ(msg2->payload().body(),std::string("hello world-2"));
//     ASSERT_EQ(mmp->push_count("queue1"),3);
//     ASSERT_EQ(mmp->waitack_count("queue1"),2);

//     mq::MessagePtr msg3=mmp->front("queue1");
//     ASSERT_EQ(msg3->payload().body(),std::string("hello world-3"));
//     ASSERT_EQ(mmp->push_count("queue1"),2);
//     ASSERT_EQ(mmp->waitack_count("queue1"),3);

//     mq::MessagePtr msg4=mmp->front("queue1");
//     ASSERT_EQ(msg4->payload().body(),std::string("hello world-4"));
//     ASSERT_EQ(mmp->push_count("queue1"),1);
//     ASSERT_EQ(mmp->waitack_count("queue1"),4);

//     mq::MessagePtr msg5=mmp->front("queue1");
//     ASSERT_EQ(msg5->payload().body(),std::string("hello world-5"));
//     ASSERT_EQ(mmp->push_count("queue1"),0);
//     ASSERT_EQ(mmp->waitack_count("queue1"),5);
// }

//恢复历史数据测试
TEST(message_test,recovery_test)
{
    ASSERT_EQ(mmp->push_count("queue1"),5);
}

//删除消息测试：确认一条消息，查看持久化消息数量，待确认消息数量
TEST(message_test,remove_test)
{
    mq::MessagePtr msg1=mmp->front("queue1");
    ASSERT_EQ(msg1->payload().body(),std::string("hello world-1"));
    ASSERT_EQ(mmp->push_count("queue1"),4);
    ASSERT_EQ(mmp->waitack_count("queue1"),1);
    mmp->ack("queue1", msg1->payload().properties().id());
    ASSERT_EQ(mmp->waitack_count("queue1"),0);
    ASSERT_EQ(mmp->durable_count("queue1"),3);
    ASSERT_EQ(mmp->total_count("queue1"),4);

}
//销毁测试
TEST(message_test,clear_test)
{
    mmp->destroyQueueMessage("queue1");
}
int main(int argc,char* argv[])
{
    testing::InitGoogleTest(&argc,argv);
    testing::AddGlobalTestEnvironment(new MessageTest);
    RUN_ALL_TESTS();
    return 0;
}