#include "../mqserver/mq_bind.hpp"
#include <gtest/gtest.h>

mq::BindingManager::ptr bmp;

class BindingTest:public testing::Environment{
    public:
        virtual void SetUp() override
        {
            bmp=std::make_shared<mq::BindingManager>("./data/meta.db");
        }
        virtual void TearDown() override
        {
            // bmp->clear();
            std::cout<<"最后的清理"<<std::endl;
        }
};

TEST(bind_test,insert_test)
{
    bmp->bind("exchange1","queue1","news.music.#",true);
    bmp->bind("exchange1","queue2","news.sport.#",true);
    bmp->bind("exchange2","queue1","news.music.cpp",true);
    bmp->bind("exchange2","queue2","news.sport.football",true);
    bmp->bind("exchange3","queue1","news.sport.ball",true);
    bmp->bind("exchange3","queue2","news.music.football",true);
    ASSERT_EQ(bmp->size(),6);
}

TEST(bind_test,select_test)
{
    ASSERT_EQ(bmp->exists("exchange1","queue1"),true);
    ASSERT_EQ(bmp->exists("exchange1","queue2"),true);
    ASSERT_EQ(bmp->exists("exchange2","queue1"),true);
    ASSERT_EQ(bmp->exists("exchange2","queue2"),true);

    mq::Binding::ptr bp =bmp->getBindings("exchange1","queue1");
    ASSERT_NE(bp.get(),nullptr);
    ASSERT_EQ(bp->exchange_name,std::string("exchange1"));
    ASSERT_EQ(bp->msgqueue_name,std::string("queue1"));
    ASSERT_EQ(bp->binding_key,std::string("news.music.#"));
}

TEST(bind_test,select_exchange_test){
    mq::MsgQueueBindingMap mbmp=bmp->getExchangeBindings("exchange1");
    ASSERT_EQ(mbmp.size(),2);
    ASSERT_NE(mbmp.find("queue1"),mbmp.end());
    ASSERT_NE(mbmp.find("queue2"),mbmp.end());

}

TEST(bind_test,remove_queue_test){
    bmp->removeMsgQueueBindings("queue1");
    ASSERT_EQ(bmp->exists("exchange1","queue1"),false);
    ASSERT_EQ(bmp->exists("exchange2","queue1"),false);
}

TEST(bind_test,remove_exchange_table){
    bmp->removeExchangeBindings("exchange1");
    ASSERT_EQ(bmp->exists("exchange1","queue1"),false);
    ASSERT_EQ(bmp->exists("exchange1","queue2"),false);
}

TEST(bind_test,remove_signal_test)
{
    ASSERT_EQ(bmp->exists("exchange2","queue2"),true);
    bmp->unBind("exchange2","queue2");
    ASSERT_EQ(bmp->exists("exchange2","queue2"),false);
}

int main(int argc,char* argv[])
{
    testing::InitGoogleTest(&argc,argv);
    testing::AddGlobalTestEnvironment(new BindingTest);
    RUN_ALL_TESTS();
    return 0;
}