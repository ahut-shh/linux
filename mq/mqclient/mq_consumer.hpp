#ifndef __M_CONSUMER_H__
#define __M_CONSUMER_H__
#include "../mqcommon/mq_helper.hpp"
#include "../mqcommon/mq_logger.hpp"
#include "../mqcommon/mq_msg.pb.h"

#include <cstdint>
#include <functional>
#include <iostream>
#include <string>
#include <unordered_map>
#include <sstream>
#include <memory>
#include <mutex>
#include <utility>
#include <vector>

namespace mq
{
    using ConsumerCallback=std::function<void(const std::string,BasicProperties *bp,const std::string)>;
    struct Consumer{
        using ptr=std::shared_ptr<Consumer>;
        std::string tag;//消费者标识
        std::string qname;//消费者订阅的队列
        bool auto_ack;//自动确认标志
        ConsumerCallback callback;//回调函数

        Consumer(){
            DLOG("new Consumer:%p",this);
        }
        Consumer(const std::string &ctag,const std::string &queue_name,bool ack,const ConsumerCallback &cb)
        :tag(ctag),qname(queue_name),auto_ack(ack),callback(cb){
            DLOG("new Channel:%p",this);
        }
        ~Consumer()
        {
            DLOG("del Channel:%p",this);
        }
    };
}
#endif