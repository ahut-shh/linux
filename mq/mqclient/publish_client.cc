#include "mq_channel.hpp"
#include "mq_connection.hpp"
#include "mq_worker.hpp"
#include <memory>
#include <string>

int main()
{
    //1、实例化异步工作线程对象
    mq::AsyncWorker::ptr awp=std::make_shared<mq::AsyncWorker>();
    //2、实例化连接
    mq::Connection::ptr conn= std::make_shared<mq::Connection>("127.0.0.1",8085,awp);
    //3、通过连接创建信道
    mq::Channel::ptr channel=conn->openChannel();
    //4、通过信道提供的服务完成所需
    //   4.1、声明一个交换机exchange1，且binding_key设置为广播模式
    google::protobuf::Map<std::string, std::string> tmp_map;
    channel->declareExchange("exchange1",mq::ExchangeType::TOPIC,true,false,tmp_map);
    //   4.2、声明一个队列queue1
    channel->declareQueue("queue1",true,false,false,tmp_map);
    //   4.3、声明一个队列queue2
    channel->declareQueue("queue2",true,false,false,tmp_map);
    //   4.4、绑定queue1-exchange1，且binding_key设置为queue1
    channel->queueBind("exchange1","queue1","queue1");
    //   4.5、绑定queue2-exchange1，且binding_key设置为news.music.#
    channel->queueBind("exchange1","queue2","news.music.#");
    //5、循环向交换机发布消息
    for(int i=0;i<10;i++)
    {
        mq::BasicProperties bp;
        bp.set_id(mq::UuidHelpr::uuid());
        bp.set_delivery_mode(mq::DeliveryMode::DURABLE);
        bp.set_routing_key("news.music.pop");
        channel->basicPublish("exchange1",&bp,"Hello world - "+std::to_string(i));
    }
    mq::BasicProperties bp;
    bp.set_id(mq::UuidHelpr::uuid());
    bp.set_delivery_mode(mq::DeliveryMode::DURABLE);
    bp.set_routing_key("news.music.sport");
    channel->basicPublish("exchange1",&bp,"Hello mq -");

    mq::BasicProperties bp1;
    bp1.set_id(mq::UuidHelpr::uuid());
    bp1.set_delivery_mode(mq::DeliveryMode::DURABLE);
    bp1.set_routing_key("news.sport.sport");
    channel->basicPublish("exchange1",&bp1,"Hello mq1 -");
    //6、关闭信道
    while(1) std::this_thread::sleep_for(std::chrono::seconds(3));
    conn->closeChannel(channel);

    return 0;
}