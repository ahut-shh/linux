#ifndef __M_HELPER_H__
#define __M_HELPER_H__

#include <iostream>
#include <string>
#include <vector>
#include <sqlite3.h>

#include <random>
#include <sstream> //字符串流
#include <iomanip>
#include <atomic>

#include <sys/stat.h>
#include <fstream>
#include <cstring>
#include <cerrno>

#include "mq_logger.hpp"

namespace mq
{
    class SqliteHelper
    {
    public:
        typedef int (*SqliteCallback)(void *, int, char **, char **);
        SqliteHelper(const std::string &dbfile) : _dbfile(dbfile), _handler(nullptr)
        {
        }
        bool open(int safe_leve = SQLITE_OPEN_FULLMUTEX) // 打开数据库
        {
            // int sqlite3_open_v2(const char *filename, sqlite3 **ppDb, int flags, const char *zVfs );
            int ret = sqlite3_open_v2(_dbfile.c_str(), &_handler, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | safe_leve, nullptr);
            if (ret != SQLITE_OK)
            {
                ELOG("创建/打开sqlite数据库失败: %s", sqlite3_errmsg(_handler));
                return false;
            }
            return true;
        }
        bool exec(const std::string &sql, SqliteCallback cb, void *arg) // 执行语句
        {
            // int sqlite3_exec(sqlite3*, char *sql, int(*callback)(void*,int,char**,char**),void* arg, char **err)
            int ret = sqlite3_exec(_handler, sql.c_str(), cb, arg, nullptr);
            if (ret != SQLITE_OK)
            {
                ELOG("%s \n 执行语句失败:%s", sql.c_str(), sqlite3_errmsg(_handler));
                return false;
            }
            return true;
        }
        void close() // 关闭数据  库
        {
            // int sqlite3_close_v2(sqlite3*)
            if (_handler)
                sqlite3_close_v2(_handler);
        }

    private:
        std::string _dbfile;
        sqlite3 *_handler;
    };

    class StrHelper
    {
    public:
        static size_t split(const std::string str, const std::string sep, std::vector<std::string> &result)
        {
            // news....music.#.pop
            // 分割思想：
            // 1、从0位置开始查找指定字符的位置，找到之后分离
            // 2、从上层查找的位置继续向后查找指定字符
            size_t pos, idx = 0;
            while (idx < str.size())
            {
                pos = str.find(sep, idx);
                if (pos == std::string::npos)
                {
                    // 没有找到,则从当前位置截取到字串末尾
                    result.push_back(str.substr(idx));
                    return result.size();
                }
                if (pos == idx)
                {
                    idx += sep.size();
                    continue;
                }
                std::string tmp = str.substr(idx, pos - idx);
                result.push_back(tmp);
                idx = pos + sep.size();
            }
            return result.size();
        }
    };

    class UuidHelpr
    {
    public:
        static std::string uuid()
        {
            std::random_device rd;
            // size_t num=rd();//生成的是一个机器随机数，效率较低
            // 因此解决方案：通过一个机器随机数作为生成的伪随机数种子
            std::mt19937_64 gengertor(rd()); // 通过梅森旋转算法，生成一个伪随机数
            // 我们要生成一个0~255之间的数字，所有要限定数字区间
            std::uniform_int_distribution<int> distribution(0, 255);
            // 然后将生成的数字转换为16进制的数字字符
            std::stringstream ss;
            // 头文件：iomanip,  setw设置位宽，setfill设置填充数据
            for (int i = 0; i < 8; i++)
            {
                ss << std::setw(2) << std::setfill('0') << std::hex << distribution(gengertor);
                if (i == 3 || i == 5 || i == 7)
                {
                    ss << "-";
                }
            }
            static std::atomic<size_t> seq(1); // 定义一个原子类型整数，初始化为1
            size_t num = seq.fetch_add(1);
            for (int i = 7; i >= 0; i--)
            {
                ss << std::setw(2) << std::setfill('0') << std::hex << ((num >> i * 8) & 0xff);
                if (i == 6)
                    ss << "-";
            }
            return ss.str();
        }
    };

    class FileHelper
    {
    public:
        FileHelper(const std::string &filename) : _filename(filename) {}
        bool exists()
        {
            struct stat st; // 如果文件存在，就一定能获取文件的属性
            return (stat(_filename.c_str(), &st)) == 0;
        }
        size_t size()
        {
            struct stat st;
            int ret = (stat(_filename.c_str(), &st));
            if (ret < 0)
            {
                return 0;
            }
            return st.st_size;
        }
        bool read(char *body, size_t offset, size_t len) // 指定位置读取指定长度
        {
            // 1、打开文件
            std::ifstream ifs(_filename, std::ios::binary | std::ios::in);
            if (ifs.is_open() == false)
            {
                ELOG("%s 文件打开失败！", _filename.c_str());
                return false;
            }
            // 2、跳转文件读取位置
            ifs.seekg(offset, std::ios::beg);
            // 3、读取文件数据
            ifs.read(body, len);
            if (ifs.good() == false)
            { // good()查看上一个操作是否执行成功
                ELOG("%s 文件读取数据失败！！", _filename.c_str());
                ifs.close();
                return false;
            }
            // 4、关闭文件
            ifs.close();
            return true;
        }
        bool read(std::string &body)
        {
            // 获取文件大小，根据文件大小调整body的空间
            size_t fsize = this->size();
            body.resize(fsize);
            return read(&body[0], 0, fsize);
        }
        bool write(const char *body, size_t offset, size_t len)
        {
            // 1、打开文件
            std::fstream fs(_filename, std::ios::binary | std::ios::out);
            if (fs.is_open() == false)
            {
                return false;
            }
            // 2、跳转到文件指定位置
            fs.seekg(offset, std::ios::beg);
            // 3、写入数据
            fs.write(body, len);
            if (fs.good() == false)
            {
                ELOG("%s 文件写入数据失败!", _filename.c_str());
                fs.close();
                return false;
            }
            // 4、关闭文件
            fs.close();
            return true;
        }
        bool write(const std::string &body)
        {
            return write(body.c_str(), 0, body.size());
        }
        bool rename(const std::string& path)
        {
            return (::rename(_filename.c_str(),path.c_str()))==0;
        }
        static bool createFile(const std::string &filename)
        {
            std::fstream ofs(filename, std::ios::binary | std::ios::out);
            if (ofs.is_open() == false)
            {
                ELOG("%s 文件创建失败！%s",filename.c_str(),strerror(errno));
                return false;
            }
            ofs.close();
            return true;
        }
        static bool removeFile(const std::string &filename)
        {
            return (::remove(filename.c_str())==0);
        }
        static bool createDirectory(const std::string &path)
        {
            //aaa/bbb/ccc/ddd
            //在多级路径创建中，我们需要从第一个父级目录开始创建
            size_t pos,idx=0;
            while(idx<path.size())
            {
                pos=path.find("/",idx);
                if(pos==std::string::npos)
                {
                    return mkdir(path.c_str(),0775)==0;
                }
                std::string subpath=path.substr(0,pos);
                int ret=mkdir(subpath.c_str(),0775);
                if(ret!=0 && errno!=EEXIST)
                {
                    ELOG("创建目录 %s 失败 %s",subpath.c_str(),strerror(errno));
                    return false;
                }
                idx=pos+1;
            }
            return true;
        }
        static bool removeDirectory(const std::string &path)
        {
            //rm -rf
            //system
            std::string cmp="rm -rf "+path;
            return system(cmp.c_str())!=-1; 
        }
        static std::string parentDirectory(const std::string &filename)
        {
            //aaa/bbb/ccc/ddd/test.txt
            size_t pos=filename.find_last_of("/");
            if(pos==std::string::npos)
            {
                return "./";
            }
            std::string path=filename.substr(0,pos);
            return path;
        }

    private:
        std::string _filename;
    };
}
#endif