#include <iostream>
#include <gtest/gtest.h>
#include <unordered_map>

using namespace std;

class MyTest : public testing::Test
{
public:
    static void SetUpTestCase()
    {
        cout << "所有单元测试前执行，初始化环境！" << endl;
        //假设我有一个全局的测试数据容器，在这里插入公共的测试数据
    }
    void SetUp() override
    {
        //在这里插入每个单元独立所需的测试数据
        cout << "单元测试初始化！" << endl;
        _mymap.insert(make_pair("hello", "你好"));
        _mymap.insert(make_pair("bye", "再见"));
    }
    static void TearDownTestCase()
    {
        //清理公共的测试数据
        cout << "所有单元测试完毕执行，清理总环境！" << endl;
    }
    void TearDown() override
    {
        //在这里清理每个单元自己插入的数据
        _mymap.clear();
        cout << "单元测试环境清理！" << endl;
    }

public:
    unordered_map<string, string> _mymap;
};
TEST_F(MyTest, insert_test)
{
    _mymap.insert(make_pair("word", "单词"));
    ASSERT_EQ(_mymap.size(), 3);
}
TEST_F(MyTest, size_test)
{
    ASSERT_EQ(_mymap.size(), 2);
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
    return 0;
}