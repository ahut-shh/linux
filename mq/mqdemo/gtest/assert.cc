#include <iostream>
#include<gtest/gtest.h>

using namespace std;
/*
    断言宏的使用
            ASSERT_ 断言失败后退出
            EXPECT_ 断言失败继续运行
    注意：
        断言宏必须在单元测试宏函数中使用
*/

//测试宏函数
TEST(test,great_than)
{
    int age=20;
    ASSERT_GT(age,18);
    printf("OK!\n");
}

int main(int argc,char*argv[])
{
    testing::InitGoogleTest(&argc,argv);
    RUN_ALL_TESTS();
    return 0;
}