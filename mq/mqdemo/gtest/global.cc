#include <iostream>
#include <gtest/gtest.h>

using namespace std;

class MyEnvironment :public testing::Environment
{
    public:
        virtual void SetUp() override
        {
            cout<<"单元测试执行前的环境初始化！"<<endl;
        }
        virtual void TearDown() override
        {
            cout<<"单元测试执行完毕后的环境清理！"<<endl;
        }
};

TEST(MyEnvironment,test1)
{
    cout<<"单元测试1"<<endl;
}

TEST(MyEnvironment,test2)
{
    cout<<"单元测试2"<<endl;
}

int main(int argc,char*argv[])
{
    testing::InitGoogleTest(&argc,argv);
    testing::AddGlobalTestEnvironment(new MyEnvironment);

    RUN_ALL_TESTS();
    return 0;
}
