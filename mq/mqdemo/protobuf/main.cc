#include"contacts.pb.h"
#include<string>
#include<iostream>
using namespace std;

int main()
{
    contacts::contact conn;
    conn.set_sn(10001);
    conn.set_name("小小");
    conn.set_score(67.5);

    //持久化数据就防止str对象，这时可以对str持久化或网络传输
    string str = conn.SerializeAsString();


    contacts::contact stu;
    bool ret = stu.ParseFromString(str);

    if(ret==false)
    {
        cout<<"反序列化失败"<<endl;
        return -1;
    }
    cout<<stu.sn()<<endl;
    cout<<stu.name()<<endl;
    cout<<stu.score()<<endl;
    return 0;
}