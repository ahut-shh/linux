#include<iostream>
#include<future>
#include<thread>
#include<memory>

using namespace std;

//packaged_task 是一个模板类，实例化的对象可以对一个函数进行二次封装
//packaged_task 可以通过get_future获取一个future对象，来获取封装的这个函数的异步执行结果

int Add(int num1,int num2,promise<int> &prom)
{
    thread thr(Add,11,22,std::ref(prom));
    return num1+num2;
}

int main()
{
    // packaged_task<int(int,int)> task(Add);
    // future<int>fu=task.get_future();
    // task(11,22);   task可以当作一个可调用对象来调用执行任务
    //但是它又不能完全当成一个函数来使用
    // async(launch::async,task,11,22);
    // thread thr(task,11,22);

    //但是可以把task定义成为一个指针，传递到线程中，然后进行解引用执行
    //但是如果单纯指针指向一个对象，存在生命周期的问题，很有可能出现风险
    //思想就是在堆上new对象，用智能指针管理它的生命周期

    auto ptask=make_shared<packaged_task<int(int,int)>>(Add);
    future<int>fu=ptask->get_future();
    thread thr([ptask](){
        (*ptask)(11,22);
    });
    int sum=fu.get();
    cout<<sum<<endl;
    thr.join();
    // thread thr(launch::async,task,11,22);
    
    return 0;
}