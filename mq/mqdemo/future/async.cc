#include<iostream>
#include<future>
#include<thread>

using namespace std;

int Add(int num1,int num2)
{
    cout<<"加法！\n";
    return num1+num2;
}

int main()
{
    //std::async(func,...)   std::async(policy,func,...)
    cout<<"-------------1---------------"<<endl;
    // std::future<int> result = std::async(launch::deferred,Add,1,2);
    std::future<int> result = std::async(launch::async,Add,1,2);
    this_thread::sleep_for(std::chrono::seconds(1));
    cout<<"-------------2---------------"<<endl;
    int sum = result.get();
    cout<<"-------------3---------------"<<endl;
    cout<<sum<<endl;

    return 0;
}