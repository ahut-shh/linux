#include<iostream>
#include<future>
#include<thread>

using namespace std;

void Add(int num1,int num2,promise<int> &prom)
{
    prom.set_value(num1+num2);
    return ;
}

int main()
{
    promise<int> prom;

    future<int> fu=prom.get_future();

    thread thr(Add,11,22,std::ref(prom));

    int res=fu.get();
    cout<<res<<endl;
    thr.join();
    return 0;
}