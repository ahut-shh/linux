#include <iostream>
#include <functional>
#include <memory>
#include <thread>
#include <mutex>
#include <vector>
#include <future>
#include <condition_variable>

using namespace std;

class threadpool
{
public:
    using Functor = std::function<void(void)>; // 使用using声明一个函数类型

    threadpool(int thr_count = 1)
    {
        _stop = false;
        for (int i = 0; i < thr_count; i++)
        {
            _threads.emplace_back(std::bind(&threadpool::entry, this));
        }
    }
    ~threadpool()
    {
        stop();
    }

    // 入队的是函数
    // push传入的首先是一个函数--用户要执行的函数，接下来是不定参，表示要处理的数据也就是要传入到函数中的参数
    // push函数内部，会将这个传入的函数封装成一个异步任务（package_task），抛入到任务池中，由工作线程取出进行执行
    template <typename F, typename... Args>
    auto push(F &&func, Args &&...args) -> future<decltype(func(args...))>
    {
        // 1、将传入的函数封装成packaged_task任务
        using return_type = decltype(func(args...));                  // 返回类型
        auto fc = bind(forward<F>(func), forward<Args>(args)...);   // 函数
        auto ptask = make_shared<packaged_task<return_type()>>(fc); // 绑定后就不需要参数
        future<return_type> fu = ptask->get_future();

        // 2、构造一个匿名函数（捕获任务对象），函数内执行任务对象
        {
            unique_lock<mutex> lock(_mutex); // 锁的管理器
            // 3、将构造处理的匿名函数对象，抛入到任务池中
            _taskpool.push_back([ptask]()
                                {
                                    (*ptask)(); // ptask是一个指向函数的指针，而(*ptask)是对该指针的解引用，即获取它指向的函数。最后的();表示调用这个函数。
                                });
            _cv.notify_one(); // 唤醒消费者
        }
        return fu;
    }

    void stop()
    {
        if(_stop==true)return;
        _stop = true;
        _cv.notify_all(); // 唤醒所有的线程
        for (auto &thread : _threads)
        {
            thread.join();
        }
    }

private:
    // 线程入口函数 ---内部不断的从任务池中取出任务进行执行
    void entry()
    {
        while (!_stop)
        {
            vector<Functor> tmp_taskpool;
            {
                // 加锁
                unique_lock<mutex> lock(_mutex); // 锁的管理器
                // 等待任务池不为空，或者_stop被置位
                _cv.wait(lock, [this]()
                         { return !_taskpool.empty() || _stop; });
                // 取出任务执行
                tmp_taskpool.swap(_taskpool); // 数据交换
            }
            for (auto &task : tmp_taskpool)
            {
                task();
            }
        }
    }

private:
    atomic<bool> _stop;
    vector<Functor> _taskpool; // 任务池
    mutex _mutex;              // 锁
    condition_variable _cv;    // 条件变量
    vector<thread> _threads;   // 线程
};