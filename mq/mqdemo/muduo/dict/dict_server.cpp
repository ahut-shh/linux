#include "muduo/net/TcpServer.h"
#include "muduo/net/EventLoop.h"
#include "muduo/net/TcpConnection.h"

#include<iostream>
#include<functional>
#include<unordered_map>

using namespace std;

class TranslateServer
{
public:
    TranslateServer(int port):_server(&_baseloop
    ,muduo::net::InetAddress("0.0.0.0",port)
    ,"TranslateServr"
    ,muduo::net::TcpServer::kReusePort)
    {
        //将我们的类成员函数，设置为服务器的回调处理函数

        //bind函数适配器是一个函数适配器，对指定函数进行参数绑定；
        _server.setConnectionCallback(std::bind(&TranslateServer::onConnection,this,std::placeholders::_1));
        _server.setMessageCallback(std::bind(&TranslateServer::onMessage,this,
            std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
    }

    //启动服务器
    void start()
    {
        _server.start();//开始事件监听
        _baseloop.loop();//开始事件监控，这时一个死循环阻塞接口，
    }
private:

    //onConnection,是在一个连接建立成功以及关闭的时候被调用
    void onConnection(const muduo::net::TcpConnectionPtr&conn){
        //新连接建立成功时的回调函数
        if(conn->connected()==true)
        {
            cout<<"新连接建立成功！"<<endl;
        }
        else{
            cout<<"新连接关闭！"<<endl;
        }
    }

    string translate(const string&str)
    {
        std::unordered_map<string,string> dict_map={
            {"hello","你好"},
            {"你好","hello"},
            {"apple","苹果"},
            {"苹果","apple"}
        };
        auto it=dict_map.find(str);
        if(it==dict_map.end())
        {
            return "没听懂！";
        }
        return it->second;
    }
    void onMessage(const muduo::net::TcpConnectionPtr&conn,muduo::net::Buffer*buf,muduo::Timestamp)
    {
        //通信连接收到请求时的回调函数
        //1、从buf中把请求的数据取出来
        string str=buf->retrieveAllAsString();
        //2、调用translate接口进行翻译
        string resp=translate(str);
        //3、向客户端进行响应结果
        conn->send(resp);

    }
private:
    //_baseloop是epoll事件监控，会进行描述符的事件监控，触发事件后进行io操作
    muduo::net::EventLoop _baseloop;
    //server对象，注意用于设置回调函数，用于告诉服务器收到什么请求该如何处理
    muduo::net::TcpServer _server;
};

int main()
{
    TranslateServer server(8085);
    server.start();
    return 0;
}