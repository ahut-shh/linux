#include "muduo/proto/codec.h"
#include "muduo/proto/dispatcher.h"

#include "muduo/base/Logging.h"
#include "muduo/base/Mutex.h"
#include "muduo/net/EventLoop.h"
#include "muduo/net/TcpServer.h"
#include "muduo/net/EventLoop.h"

#include "request.pb.h"
#include <iostream>
#include <unordered_map>
using namespace std;

class Server
{
public:
    typedef std::shared_ptr<google::protobuf::Message> MessagePtr;
    typedef std::shared_ptr<bit::TranslateRequest> TranslateRequestPtr;
    typedef std::shared_ptr<bit::TranslateResponse> TranslateResponsePtr;
    typedef std::shared_ptr<bit::AddRequest> AddRequestPtr;
    typedef std::shared_ptr<bit::AddResponse> AddResponsePtr;
    Server(int port)
        : _server(&_baseloop, muduo::net::InetAddress("0.0.0.0", port)
        , "Server", muduo::net::TcpServer::kReusePort)
        , _dispatcher(std::bind(&Server::onUnknownMessage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3))
        , _codec(std::bind(&ProtobufDispatcher::onProtobufMessage, &_dispatcher, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3))
    {
        // 注册业务请求处理函数
        _dispatcher.registerMessageCallback<bit::TranslateRequest>(std::bind(&Server::onTranslate, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

        _dispatcher.registerMessageCallback<bit::AddRequest>(std::bind(&Server::onAdd, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

        _server.setConnectionCallback(std::bind(&Server::onConnection, this, std::placeholders::_1));

        _server.setMessageCallback(std::bind(&ProtobufCodec::onMessage, &_codec, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    }

    void start()
    {
        _server.start();
        _baseloop.loop();
    }

private:
    string translate(const string &str)
    {
        std::unordered_map<string, string> dict_map = {
            {"hello", "你好"},
            {"你好", "hello"},
            {"apple", "苹果"},
            {"苹果", "apple"}};
        auto it = dict_map.find(str);
        if (it != dict_map.end())
        {
            return it->second;
        }
        return "没有找到";
    }
    void onTranslate(const muduo::net::TcpConnectionPtr &conn, const TranslateRequestPtr &message, muduo::Timestamp)
    {
        // 1、提取message中的有效消息，也就是要翻译的内容
        string req_msg = message->msg();
        // 2、进行翻译，得到结果
        string rsp_msg = translate(req_msg);
        // 3、组织protobuf的响应
        bit::TranslateResponse resp;
        resp.set_msg(rsp_msg);
        // 4、发送消息
        _codec.send(conn, resp);
    }
    void onAdd(const muduo::net::TcpConnectionPtr &conn, const AddRequestPtr &message, muduo::Timestamp)
    {
        int num1 = message->num1();
        int num2 = message->num2();
        int result = num1 + num2;
        bit::AddResponse resp;
        resp.set_result(result);
        _codec.send(conn, resp);
    }
    void onUnknownMessage(const muduo::net::TcpConnectionPtr &conn, const MessagePtr &message, muduo::Timestamp)
    {
        LOG_INFO << "onUnknownMessage: " << message->GetTypeName();
        conn->shutdown();
    }
    void onConnection(const muduo::net::TcpConnectionPtr &conn)
    {
        if (conn->connected())
        {
            LOG_INFO << "新连接建立成功！";
        }
        else
        {
            LOG_INFO << "连接即将关闭！";
        }
    }

private:
    muduo::net::EventLoop _baseloop;
    muduo::net::TcpServer _server;  // 服务器
    ProtobufDispatcher _dispatcher; // 请求分发器对象
    ProtobufCodec _codec;           // protobuf协议处理器
};

int main()
{
    Server server(8085);
    server.start();
    return 0;
}