#include "muduo/proto/codec.h"
#include "muduo/proto/dispatcher.h"

#include "muduo/base/Logging.h"
#include "muduo/base/Mutex.h"
#include "muduo/net/EventLoopThread.h"
#include "muduo/net/TcpClient.h"

#include "request.pb.h"
#include "muduo/net/TcpConnection.h"
#include "muduo/base/CountDownLatch.h"
#include<iostream>
using namespace std;

class Client
{
public:
    typedef std::shared_ptr<google::protobuf::Message> MessagePtr;
    typedef std::shared_ptr<bit::TranslateResponse> TranslateResponsePtr;
    typedef std::shared_ptr<bit::AddResponse> AddResponsePtr;
    Client(const string &sip,int sport):_latch(1)
    ,_client(_loopthread.startLoop(),muduo::net::InetAddress(sip,sport),"Client")
    ,_dispatcher(std::bind(&Client::onUnknownMessage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3))
    , _codec(std::bind(&ProtobufDispatcher::onProtobufMessage, &_dispatcher, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3))
    {
          // 注册业务请求处理函数
        _dispatcher.registerMessageCallback<bit::TranslateResponse>(std::bind(&Client::onTranslate, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

        _dispatcher.registerMessageCallback<bit::AddResponse>(std::bind(&Client::onAdd, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

        _client.setConnectionCallback(std::bind(&Client::onConnection, this, std::placeholders::_1));

        _client.setMessageCallback(std::bind(&ProtobufCodec::onMessage, &_codec, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
   
    }
     // 连接服务器 ---需要阻塞等待连接建立成功之后再返回
    void connect()
    {
        _client.connect();
        _latch.wait(); // 阻塞等待，直到连接建立成功
    }
    void Translate(const string &msg)
    {
        bit::TranslateRequest req;
        req.set_msg(msg);//序列化
        send(&req);

    }
    void Add(int num1,int num2)
    {
        bit::AddRequest req;
        req.set_num1(num1);//序列化
        req.set_num2(num2);//序列化
        send(&req);
    }

    // 发送数据
    bool send(const google::protobuf::Message *msg)
    {
        if (_conn->connected()) // 连接状态正常再发送，否则就false
        {
           _codec.send(_conn,*msg);//中间进行了反序列化
            return true;
        }
        return false;
    }
private:
    void onTranslate(const muduo::net::TcpConnectionPtr &conn, const TranslateResponsePtr &message, muduo::Timestamp)
    {
        cout<<"翻译结果："<<message->msg()<<endl;
    }
    void onAdd(const muduo::net::TcpConnectionPtr &conn, const AddResponsePtr &message, muduo::Timestamp)
    {
        cout<<"加法结果："<<message->result()<<endl;

    }

    void onConnection(const muduo::net::TcpConnectionPtr &conn)
    {
        if (conn->connected())
        {
            _latch.countDown(); // 唤醒主线程中的阻塞
            _conn = conn;
        }
        else
        {
            // 连接关闭的操作
            _conn.reset();
            cout << "连接关闭" << endl;
        }
    }

    void onUnknownMessage(const muduo::net::TcpConnectionPtr &conn, const MessagePtr &message, muduo::Timestamp)
    {
        LOG_INFO << "onUnknownMessage: " << message->GetTypeName();
        conn->shutdown();
    }
private:
    muduo::CountDownLatch _latch;
    muduo::net::EventLoopThread _loopthread; // 直接进行的，不需要loop了
    muduo::net::TcpConnectionPtr _conn;
    muduo::net::TcpClient _client;
    ProtobufDispatcher _dispatcher; // 请求分发器对象
    ProtobufCodec _codec;           // protobuf协议处理器
};

int main()
{
    Client client("127.0.0.1",8085);
    client.connect();

    client.Translate("hello");
    client.Add(11,22);

    sleep(2);
    return 0;
}