#include "muduo/net/TcpServer.h"
#include "muduo/net/EventLoop.h"
#include "muduo/net/TcpConnection.h"
#include <iostream>
#include<functional>
#include<unordered_map>
using namespace std;

class TranslateSerever
{
public:
    TranslateSerever(int port):_server(&_baseloop,muduo::net::InetAddress("0.0.0.0",port),"TranslateServer",muduo::net::TcpServer::kReusePort)
    {
        _server.setConnectionCallback(bind(&TranslateSerever::onConnection,this,std::placeholders::_1));
        _server.setMessageCallback(bind(&TranslateSerever::onMessage,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
    }
    //启动服务器
    void start()
    {
        _server.start();
        _baseloop.loop();
    }
private:
    void onConnection(const muduo::net::TcpConnectionPtr &conn)
    {
        if(conn->connected())
        {
            cout<<"新连接建立成功！"<<endl;
        }
        else
        {
            cout<<"关闭新连接！"<<endl;
        }
    }
    string translate(const string&str)
    {
        std::unordered_map<string,string> dict_map={
            {"hello","你好"},
            {"你好","hello"},
            {"apple","苹果"},
            {"苹果","apple"}
        };
        auto it=dict_map.find(str);
        if(it!=dict_map.end())
        {
            return it->second;
        }
        return "没有找到";
    }
    void onMessage(const muduo::net::TcpConnectionPtr &conn,muduo::net::Buffer* buf,muduo::Timestamp)
    {
        string str = buf->retrieveAllAsString();
        string resp=translate(str);
        conn->send(resp);
    }

private:
    muduo::net::EventLoop _baseloop;
    muduo::net::TcpServer _server;
};

int main()
{
    TranslateSerever server(8085);
    server.start();
    return 0;
}