#include <iostream>
#include <random>
#include <sstream> //字符串流
#include <iomanip>
#include <atomic>

static std::string uuid()
{
    std::random_device rd;
    // size_t num=rd();//生成的是一个机器随机数，效率较低
    // 因此解决方案：通过一个机器随机数作为生成的伪随机数种子
    std::mt19937_64 gengertor(rd()); // 通过梅森旋转算法，生成一个伪随机数
    // 我们要生成一个0~255之间的数字，所有要限定数字区间
    std::uniform_int_distribution<int> distribution(0, 255);
    // 然后将生成的数字转换为16进制的数字字符
    std::stringstream ss;
    // 头文件：iomanip,  setw设置位宽，setfill设置填充数据
    for (int i = 0; i < 8; i++)
    {
        ss << std::setw(2) << std::setfill('0') << std::hex << distribution(gengertor);
        if (i == 3 || i == 5 || i == 7)
        {
            ss << "-";
        }
    }
    static std::atomic<size_t> seq(1); // 定义一个原子类型整数，初始化为1
    size_t num = seq.fetch_add(1);
    for (int i = 7; i >= 0; i--)
    {
        ss << std::setw(2) << std::setfill('0') << std::hex << ((num >> i * 8) & 0xff);
        if (i == 6)
            ss << "-";
    }
    return ss.str();
}
int main()
{
    for(int i=0;i<20;i++)
    {
        std::cout<<uuid()<<std::endl;
    }

    // std::random_device rd;
    // // size_t num=rd();//生成的是一个机器随机数，效率较低
    // // 因此解决方案：通过一个机器随机数作为生成的伪随机数种子
    // std::mt19937_64 gengertor(rd()); // 通过梅森旋转算法，生成一个伪随机数
    // // 我们要生成一个0~255之间的数字，所有要限定数字区间
    // std::uniform_int_distribution<int> distribution(0, 255);
    // // 然后将生成的数字转换为16进制的数字字符
    // std::stringstream ss;
    // // 头文件：iomanip,  setw设置位宽，setfill设置填充数据
    // for (int i = 0; i < 8; i++)
    // {
    //     ss << std::setw(2) << std::setfill('0') << std::hex << distribution(gengertor);
    //     if (i == 3 || i == 5 || i == 7)
    //     {
    //         ss << "-";
    //     }
    // }
    // static std::atomic<size_t> seq(1); // 定义一个原子类型整数，初始化为1
    // size_t num = seq.fetch_add(1);
    // for (int i = 7; i >= 0; i--)
    // {
    //     ss << std::setw(2) << std::setfill('0') << std::hex << ((num >> i * 8) & 0xff);
    //     if (i == 6)
    //         ss << "-";
    // }
    // std::cout << ss.str() << std::endl;
    return 0;
}