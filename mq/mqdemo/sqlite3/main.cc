#include"sqlite.hpp"
#include<cassert>

int select_stu_callback(void*arg,int col_count,char**result,char**fieids_name)
{
    vector<string> *array=(vector<string>*)arg;
    array->push_back(result[0]);
    return 0;
}

int main()
{
    SqliteHelper helper("./test.db");
    //1、创建/打开库文件
    assert(helper.open());
    //2、创建表（不存在则创建）
    const char *ct="create table if not exists student(sn int primary key,name varchar(32),age int);";
    assert(helper.exec(ct,nullptr,nullptr));
    //3、新增数据，修改，删除，查询
    // const char *insert_sql="insert into student values(1,'小明',12),(2,'小黑',14),(3,'小红',15);";
    // assert(helper.exec(insert_sql,nullptr,nullptr));

    const char *select_sql="select name from student;";
    vector<string> arry;

    assert(helper.exec(select_sql,select_stu_callback,&arry));
    for (auto &name:arry)
    {
        cout<<name<<endl;
    }
    
    //4、关闭数据库
    helper.close();
    return 0;
}