#include<iostream>
#include<string>
#include<vector>

size_t split(const std::string str,const std::string sep,std::vector<std::string>&result)
{
    //news....music.#.pop
    //分割思想：
    //1、从0位置开始查找指定字符的位置，找到之后分离
    //2、从上层查找的位置继续向后查找指定字符
    size_t pos,idx=0;
    while(idx<str.size())
    {
        pos=str.find(sep,idx);
        if(pos==std::string::npos)
        {
            //没有找到,则从当前位置截取到字串末尾
            result.push_back(str.substr(idx));
            return result.size();
        }
        if(pos==idx){
            idx+=sep.size();
            continue;
        }
        std::string tmp=str.substr(idx,pos-idx);
        result.push_back(tmp);
        idx=pos+sep.size();
    }
    return result.size();
}

int main()
{
    std::string str="news....music.#.pop";
    std::vector<std::string> array;
    int n=split(str,".",array);
    for(auto&s:array)
    {
        std::cout<<s<<std::endl;
    }
    return 0;
}