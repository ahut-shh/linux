#ifndef __M_HOST_H__
#define __M_HOST_H__
#include "mq_exchange.hpp"
#include "mq_queue.hpp"
#include "mq_message.hpp"
#include "mq_bind.hpp"

#include <memory>
#include <string>

namespace mq{
    
    class VirtualHost{
        public:
            using ptr=std::shared_ptr<VirtualHost>;
            VirtualHost(const std::string &hostname,const std::string &basedir,const std::string &dbfile)
            :_host_name(hostname),
            _emp(std::make_shared<ExchangeManager>(dbfile)),
            _mqmp(std::make_shared<MsgQueueManager>(dbfile)),
            _bmp(std::make_shared<BindingManager>(dbfile)),
            _mmp(std::make_shared<MessageManager>(basedir)){
                //获取所有队列信息，通过队列名称恢复历史消息数据
                QueueMap qm=_mqmp->allQueues();
                for(auto &q:qm)
                {
                    _mmp->initQueueMessage(q.first);
                }
            }

            bool declareExchange(const std::string &name, ExchangeType type, bool durable, bool auto_delete,
            const google::protobuf::Map<std::string, std::string>  &args){
                return _emp->declareExchange(name,type,durable,auto_delete,args);
            }
            
            bool deleteExchange(const std::string &name)
            {
                //删除交换机的时候，需要将交换机相关的绑定信息删除掉
                _bmp->removeExchangeBindings(name);
                return _emp->deleteExchange(name);
            }

            bool existsExchange(const std::string &name){
                return _emp->exists(name);
            }

            Exchange::ptr selectExchange(const std::string ename){
                return _emp->selectExchange(ename);
            }

            bool declareQueue(const std::string &qname, bool qdurable,bool qexclusive,bool qauto_delete,
            const google::protobuf::Map<std::string, std::string>  &qargs){
                //初始化队列的消息句柄（消息的存储管理）
                //队列的创建
                _mmp->initQueueMessage(qname);
                return _mqmp->declareQueue(qname, qdurable, qexclusive, qauto_delete, qargs);
            }

            bool deleteQueue(const std::string &name){
                //删除时队列的相关的数据有两个：队列的消息，队列的绑定信息
                _mmp->destroyQueueMessage(name);//删除的是队列中的消息
                _bmp->removeMsgQueueBindings(name);//删除的是队列的绑定信息
                return _mqmp->deleteQueue(name);//删除队列
            }
            bool existsQueue(const std::string &name){
                return _mqmp->exists(name);
            }

            QueueMap allQueues(){
                return _mqmp->allQueues();
            }

            bool bind(const std::string &ename,const std::string &qname,const std::string &key){
                Exchange::ptr ep=_emp->selectExchange(ename);
                if(ep.get()==nullptr)
                {
                    DLOG("进行队列绑定失败，交换机%s不存在！",ename.c_str());
                    return false;
                }
                MsgQueue::ptr mp=_mqmp->selectQueue(qname);
                if(mp.get()==nullptr)
                {
                    DLOG("进行队列绑定失败，队列%s不存在！",qname.c_str());
                    return false;
                }
                //两个都是持久化的绑定信息才是持久化的
                return _bmp->bind(ename, qname, key, ep->durable && mp->durable);
            }

            void unBind(const std::string &ename,const std::string &qname){
                return _bmp->unBind(ename, qname);
            }

            bool existsBinding(const std::string &ename,const std::string &qname){
                return _bmp->exists(ename,qname);
            }

            //获取指定交换机的所有的绑定信息，队列，binding_key
            MsgQueueBindingMap exchangeBindings(const std::string &ename)
            {
                return _bmp->getExchangeBindings(ename);
            }


            //新增一条消息
            bool basicPublish(const std::string &qname,BasicProperties *bp,const std::string &body){
                MsgQueue::ptr mp=_mqmp->selectQueue(qname);
                if(mp.get()==nullptr)
                {
                    DLOG("发布消息失败，队列%s不存在！",qname.c_str());
                    return false;
                }
                return _mmp->insert(qname, bp,body,mp->durable);
            }
            //消费一条消息
            MessagePtr basicConsume(const std::string &qname){
                return _mmp->front(qname);
            }
            //确认消息
            bool basicAck(const std::string &qname,const std::string &msgid){
                return _mmp->ack(qname, msgid);
            }

            void clear()
            {
                _emp->clear();
                _mqmp->clear();
                _bmp->clear();
                _mmp->clear();
            }
        private:
            std::string _host_name;
            ExchangeManager::ptr _emp;
            MsgQueueManager::ptr _mqmp;
            BindingManager::ptr _bmp;
            MessageManager::ptr _mmp;
    };
}
#endif