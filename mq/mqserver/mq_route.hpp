#ifndef __M_ROUTE_H__
#define __M_ROUTE_H__

#include "../mqcommon/mq_helper.hpp"
#include "../mqcommon/mq_logger.hpp"
#include "../mqcommon/mq_msg.pb.h"

#include <iostream>
#include <string>
#include <vector>

namespace mq{
    class Router{
        public:
            static bool isLegalRoutingKey(const std::string &routing_key)
            {
                //只需要判断字符是否合法：a~z,A~Z,0~9,.,_
                for(auto &ch:routing_key)
                {
                    if((ch>='a'&&ch<='z')||(ch>='A'&&ch<='Z')||(ch>='0'&&ch<='9')||ch=='.'||ch=='_'){
                        continue;
                    }
                    return false;
                }
                return true;
            }
            static bool isLegalBindingKey(const std::string &binding_key)
            {
                //1、判断字符是否合法：a~z,A~Z,0~9,.,_，*，#
                for(auto &ch:binding_key)
                {
                    if((ch>='a'&&ch<='z')||(ch>='A'&&ch<='Z')||(ch>='0'&&ch<='9')||ch=='.'||ch=='_'||ch=='*'||ch=='#'){
                        continue;
                    }
                    return false;
                }
                //2、*和#必须独立存在 news.music#
                std::vector<std::string> sub_words;
                StrHelper::split(binding_key,".",sub_words);
                for(auto &words:sub_words){
                    if(words.size()>1&&((words.find("*")!=std::string::npos)||(words.find("#")!=std::string::npos)))
                    {
                        return false;
                    }
                }
                //3、*和#不能连续出现
                for(int i=1;i<sub_words.size();i++)
                {
                    if((sub_words[i]=="#"&&sub_words[i-1]=="*")||
                    (sub_words[i]=="#"&&sub_words[i-1]=="#")||
                    (sub_words[i]=="*"&&sub_words[i-1]=="#")){
                        return false;
                    }
                }
                return true;
            }
            static bool route(ExchangeType type,const std::string &routing_key,const std::string &binding_key)
            {
                if(type==ExchangeType::DIRECT){
                    return routing_key==binding_key;
                }
                else if(type==ExchangeType::FANOUT)
                {
                    return true;
                }
                //主题交换：进行模式匹配   news.#   &   news.music.pop
                //1、将binding_key和routing_key进行分割
                std::vector<std::string>bkeys,rkeys;
                int n_bkeys=StrHelper::split(binding_key,".", bkeys);
                int n_rkeys=StrHelper::split(routing_key,".", rkeys);
                //2、定义标记数组，并初始化[0][0]位置为true,其他位置为false;
                std::vector<std::vector<bool>> dp(n_bkeys+1,std::vector<bool>(n_rkeys+1,false));
                dp[0][0]=true;
                //3、如果binding_key以#开始，则将#对应的行置为true
                for(int i=1;i<=n_bkeys;i++)
                {
                    if(bkeys[i-1]=="#")
                    {
                        dp[i][0]=true;
                        continue;
                    }
                    break;
                }
                //4、使用routing_key中的每个单词和binding_key进行匹配并标记
                for(int i=1;i<=n_bkeys;i++)
                {
                    for(int j=1;j<=n_rkeys;j++)
                    {
                        //如果当前bkeys是*或者两个相同的单词，单词匹配成功，从左上方继承结果
                        if(bkeys[i-1]==rkeys[j-1]||bkeys[i-1]=="*"){
                            dp[i][j]=dp[i-1][j-1];
                        }
                        //如果当前bkeys是# 匹配成功，从左上方，上方，左方继承
                        if(bkeys[i-1]=="#")
                        {
                            dp[i][j]=dp[i-1][j-1]|dp[i][j-1]|dp[i-1][j];
                        }
                    }
                }
                return dp[n_bkeys][n_rkeys];
            }
    };
}
#endif