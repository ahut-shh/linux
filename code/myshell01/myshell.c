#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<errno.h>

# define SkipPath(p) do{p+=(strlen(p)-1);while(*p!='/')p--;}while(0)


#define SIZE 512
#define SEP " "
#define NUM 32
#define ZERO '\0'

char cwd[SIZE*2];
int lastcode =0;
char *argv[NUM];

const char *Getname()
{
	const char *name=getenv("USER");
	if(name==NULL)return "None";
	return name;
}


const char *Gethostname()
{
	const char *hostname=getenv("HOSTNAME");
	if(hostname==NULL)return "None";
	return hostname;
}


const char *Getcwd()
{
	const char *pwd=getenv("PWD");
	if(pwd==NULL)return "None";
	return pwd;
}

void Makecommandline()
{
	char cmd[SIZE];
	const char *name=Getname();
	const char *hostname = Gethostname();
	const char *cwd =Getcwd();
	SkipPath(cwd);
	snprintf(cmd,sizeof(cmd),"[%s@%s %s]> ",name,hostname,strlen(cwd)==1 ? "/":cwd+1);
	printf("%s",cmd);
	fflush(stdout);
}

int Getusercommand(char Usercmd[],size_t n)
{
	char *s = fgets(Usercmd,n,stdin);
	if(s==NULL)
	{
		return -1;
	}
	Usercmd[strlen(Usercmd)-1]=ZERO;
	return strlen(Usercmd);
}

void Splitcommand(char Usercmd[])
{
	argv[0]=strtok(Usercmd,SEP);
	int index=1;
	while(argv[index++]=strtok(NULL,SEP));
}
void Executecommand()
{
	pid_t id =fork();
	if(id<0)
	{
		exit(1);
	}
	else if(id==0)
	{
		execvp(argv[0],argv);
		exit(errno);
	}
	else
	{
		int status=0;
		pid_t rid =waitpid(id,&status,0);
		if(rid>0)
		{
			//wait sucess
			lastcode = WEXITSTATUS(status);
			if(lastcode!=0)
			{
				printf("%s:%s:%d\n",argv[0],strerror(lastcode),lastcode);
			}
		}
	}
}
const char *Gethome()
{
	const char *home =getenv("HOME");
	if(home==NULL)return "/root";
	return home;
}
void Cd()
{
	const char *path =argv[1];
	if(path==NULL)
	{
		//返回家目录
		path = Gethome();
	}
	chdir(path);
	char tmp[SIZE*2];
	getcwd(tmp,sizeof(tmp));
	snprintf(cwd,sizeof(cwd),"PWD=%s",tmp);
	putenv(cwd);
	//printf("%s\n",cwd);
}
int Checkbuildin()
{
	int yes=0;
	if(strcmp(argv[0],"cd")==0)
	{
		yes=1;
		Cd();
	}
	else if(strcmp(argv[0],"echo")==0&&strcmp(argv[1],"$?")==0)
	{
		yes=1;
		printf("%d\n",lastcode);
		lastcode=0;
	}
	return yes;
}
int main()
{
	while(1)
	{
		//1、我们自己输入一个命令行
		Makecommandline();

		//2、获取用户命令字符串分割
		char Usercmd[SIZE];
		int n=Getusercommand(Usercmd,sizeof(Usercmd));
		if(n<0)return 1;
		//printf("%s\n",Usercmd);
		//3、命令行字符串分割
		Splitcommand(Usercmd);
		//int i;
		//for(i=0;argv[i];i++)
		//{
		//	printf("argv[%d]:%s\n",i,argv[i]);
		//}
		//4、检测命令是否是内建命令
		n = Checkbuildin();
		if(n)continue;
		//5、执行命令
		Executecommand();
	}
	return 0;
}
