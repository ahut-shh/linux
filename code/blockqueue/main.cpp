#include "BlockQueue.hpp"
#include "task.hpp"
#include <ctime>
void *Consumer(void *args)
{
    BlockQueue<Task> *bq = static_cast<BlockQueue<Task> *>(args);
    while (true)
    {
        // 1、获取数据
        // int data = 0;
        // bq->Pop(&data);
        Task tt;
        bq->Pop(&tt);
        // 2、处理数据
        cout << "Consmer ->" <<tt.Excute()<< endl;
    }
}

void *Productor(void *args)
{
    srand(time(nullptr) ^ getpid());
    BlockQueue<Task> *bq = static_cast<BlockQueue<Task> *>(args);
    while (true)
    {
        sleep(2);
        // 1、构建数据
        // int data = rand() % 10 + 1;
        int x =rand()%10+1;
        usleep(1000);
        int y =rand()%10+1;
        Task t(x,y);
        // 2、生产数据
        bq->Equeue(t);
        cout << "Productor ->" << t.Excute() << endl;
    }
}
int main()
{
    BlockQueue<Task> *bq = new BlockQueue<Task>();

    pthread_t c, p;
    pthread_create(&c, nullptr, Consumer, bq);
    pthread_create(&p, nullptr, Productor, bq);

    pthread_join(c, nullptr);
    pthread_join(p, nullptr);

    return 0;
}