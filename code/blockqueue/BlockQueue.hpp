#pragma once

#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <queue>
#include <sys/types.h>
using namespace std;

const int defaultcap = 5;

template <typename T>
class BlockQueue
{
private:
    bool isfull()
    {
        if (_block_queue.size() == _max_cp)
            return true;
        return false;
    }
    bool isEmpty()
    {
        return _block_queue.empty();
    }

public:
    BlockQueue(int cap = defaultcap) : _max_cp(cap)
    {
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_p_cond, nullptr);
        pthread_cond_init(&_c_cond, nullptr);
    }
    ~BlockQueue()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_p_cond);
        pthread_cond_destroy(&_c_cond);
    }
    void Pop(T *out)
    {
        pthread_mutex_lock(&_mutex);
        while (isEmpty())
        {
            // 空了
            pthread_cond_wait(&_c_cond, &_mutex);
        }
        *out = _block_queue.front();
        _block_queue.pop();
        pthread_mutex_unlock(&_mutex);
        pthread_cond_signal(&_p_cond);
    }

    void Equeue(const T &in)
    {
        pthread_mutex_lock(&_mutex);
        while (isfull())
        {
            // 满了，生产者不能生产，必须等待
            // pthread_cond_wait被调用的时候，除了让自己继续排队等待，还会自己释放传入的锁
            // 被唤醒的时候，会重新排队加锁
            pthread_cond_wait(&_p_cond, &_mutex);
        }
        _block_queue.push(in);
        pthread_mutex_unlock(&_mutex);
        pthread_cond_signal(&_c_cond);
    }

private:
    queue<T> _block_queue; // 临界资源
    int _max_cp;
    pthread_mutex_t _mutex;
    pthread_cond_t _p_cond;
    pthread_cond_t _c_cond;
};