#include "namepipe.hpp"

// 客户 --->w
int main()
{
    namepipe pipe(comm_path, gUser);
    if (pipe.open_writepipe())
    {
        while (true)
        {
            cout << "please enter:";
            string message;
            getline(cin, message);
            pipe.writepipe(message);
        }
    }

    return 0;
}