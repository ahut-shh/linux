#pragma once

#include <iostream>
#include <string>
#include<cstdio>
#include<cerrno>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;

const string comm_path = "./fifo";
#define gCreater 1
#define gUser 2
#define Read O_RDONLY
#define Write O_WRONLY
#define FD -1
#define SIZE 4069
class namepipe
{
private:
    bool openpipe(int flag)
    {
        _fd = open(_path.c_str(), flag);
        if (_fd < 0)
        {
            return false;
        }
        return true;
    }

public:
    namepipe(const string &path, int who) : _path(path), _id(who),_fd(FD)
    {
        if (_id == gCreater)
        {
            int res = mkfifo(path.c_str(), 0666);
            cout << "namepipe create sucess" << endl;
        }
    }
    ~namepipe()
    {
        if (_id == gCreater)
        {
            int res = unlink(_path.c_str());
            if (res == 0)
            {
                cout << "namepipe remove sucess" << endl;
            }
            if(_fd!=FD)
            {
                close(_fd);
            }
        }
    }

    bool open_readpipe()
    {
        return openpipe(Read);
    }
    bool open_writepipe()
    {
        return openpipe(Write);
    }

    int readpipe(string * out)
    {
        char buff[SIZE];
        int n=read(_fd,buff,sizeof(buff));
        if(n>0)
        {
            buff[n]=0;
            *out=buff;
        }
        return n;
    }

    // int readpipe(string * out)
    // {
      
    //     int n=read(_fd,out,sizeof(&out));
    //     return n;
    // }
    int writepipe(const string & in)
    {
        write(_fd,in.c_str(),sizeof(in));
    }
private:
    const string _path;
    int _id;
    int _fd;
};