#include "namepipe.hpp"

// r
int main()
{
    namepipe pipe(comm_path, gCreater);
    if (pipe.open_readpipe())
    {
        while (true)
        {
            string message;
            int n = pipe.readpipe(&message);
            if (n > 0)
            {
                cout << "client say:" << message << endl;
            }
        }
    }
    return 0;
}