#pragma once

#include <iostream>
#include <ctime>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
using namespace std;

#define NUM 3

typedef void (*task_t)(); // 函数指针类型

task_t tasks[NUM];

// 创建任务
void Print()
{
    cout << "I am Print Task" << endl;
}
void Flush()
{
    cout << "I am Flush Task" << endl;
}
void Download()
{
    cout << "I am Download Task" << endl;
}

// 初始化
void Loadtask()
{
    srand(time(nullptr) ^ getpid());
    tasks[0] = Print;
    tasks[1] = Download;
    tasks[2] = Flush;
}

void Excutetask(int num)
{
    if (num < 0 || num > 2)
        return;
    tasks[num]();
}

int selecttask()
{
    return rand()%NUM;
}