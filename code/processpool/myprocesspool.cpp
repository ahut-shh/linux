#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include "test.hpp"
#include <sys/types.h>
#include <sys/wait.h>
using namespace std;

class channel
{
public:
    channel(int wfd, pid_t id, const string &name) : _wfd(wfd), childid(id), _name(name)
    {
    }
    ~channel()
    {
    }
    int getwfd() { return _wfd; }
    pid_t getid() { return childid; }
    string getname() { return _name; }

    void closechannel()
    {
        close(_wfd);
    }
    void wait()
    {
        pid_t rid =waitpid(childid,nullptr,0);
        if(rid>0)
        {
            cout<<"wait success"<<endl;
        }
    }

private:
    int _wfd;
    pid_t childid;
    string _name;
};

void work(int rfd)
{
    while (true)
    {
        int command = 0;
        int n = read(rfd, &command, sizeof(command));
        if (n == sizeof(int))
        {
            Excutetask(command);
        }
    }
}

// 1、创建子进程和信道
void create(int num, vector<channel> &channels)
{
    for (int i = 0; i < num; i++)
    {
        int pipfd[2] = {0};
        int n = pipe(pipfd);
        pid_t id = fork();
        if (id == 0)
        {
            // child --read
            close(pipfd[1]);
            work(pipfd[0]);
            close(pipfd[0]);
            exit(0);
        }
        // father --write
        close(pipfd[0]);
        string name = "channel-";
        name += to_string(i);
        channels.push_back(channel(pipfd[1], id, name));
    }
}

int selectchannel(int num)
{
    static int next = 0;
    int channel = next;
    next++;
    next %= num;
    return channel;
}

void send(int tasknum, int channel_index, vector<channel> &channels)
{
    write(channels[channel_index].getwfd(), &tasknum, sizeof(tasknum));
}

// 2、通过channel控制子进程

void controlone(vector<channel> &channels)
{
    sleep(3);
    // 2.1、选一个任务
    int tasknum = selecttask();
    // 2.2、选一个信道和进程
    int channel_index = selectchannel(channels.size());
    // 2.3、发送
    send(tasknum, channel_index, channels);

    cout << "信息发送成功" << endl;
}
void control(vector<channel> &channels, int times = -1)
{
    if (times > 0)
    {
        while (times--)
        {
            controlone(channels);
        }
    }
    else
    {
        while (true)
        {
            controlone(channels);
        }
    }
}

void clean(vector<channel> &channels)
{
    for (auto channel : channels)
    {
        channel.closechannel();
    }
    for (auto channel : channels)
    {
        channel.wait();
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cerr << argv[0] << "processnum" << endl;
        return 1;
    }
    int num = stoi(argv[1]);
    Loadtask();
    // 1、创建子进程和信道
    vector<channel> channels;
    create(num, channels);

    // 2、通过channel控制子进程
    control(channels, 10);

    // 3、回收管道和子进程:关闭所有的写端，回收子进程·      
    clean(channels);
    // for(auto channel:channels)
    // {
    //     cout<<channel.getid()<<" "<<channel.getwfd()<<" "<<channel.getname()<<endl;
    // }
    return 0;
}