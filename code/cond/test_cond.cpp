#include <iostream>
#include <pthread.h>
#include <string>
#include <unistd.h>
using namespace std;
const int num = 5;
pthread_mutex_t mutex =PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
void *Wait(void *args)
{
    string name = static_cast<char *>(args);
    while (true)
    {
        pthread_mutex_lock(&mutex);
        pthread_cond_wait(&cond,&mutex);//在条件变量下进行等待，这里就是进程等待的位置
        cout << "I am " << name << endl;
        pthread_mutex_unlock(&mutex);
        // usleep(10000);
    }
}

int main()
{
    pthread_t tid[num];
    for (int i = 0; i < num; i++)
    {
        char *buff = new char[1024];
        snprintf(buff, 1024, "thread-%d", i + 1);

        pthread_create(&tid[i], nullptr, Wait, (void *)buff);
        sleep(1);
    }

    while(true)
    {
        pthread_cond_signal(&cond);
        sleep(1);
    }

    for (int i = 0; i < num; i++)
    {
        pthread_join(tid[i], nullptr);
    }
    return 0;
}