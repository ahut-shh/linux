#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

void handle(int sig)
{
    cout<<"SIGCHLD : pid: "<<getpid()<<endl;
}
int main()
{
    signal(SIGCHLD,SIG_IGN);
    pid_t id = fork();
    if (id == 0)
    {
        // child
        int cnt = 5;
        while (true)
        {
            sleep(1);
            cout << "get a child process,pid:" << getpid() << endl;
            cnt--;
            if (cnt == 0)
            {
                sleep(10);
                exit(1);
            }
        }
    }
    // father
    sleep(200);
    waitpid(-1, nullptr, 0);
    return 0;
}

// void PrintPending(sigset_t *pending)
// {
//     for (int signo = 31; signo >= 1; signo--)
//     {
//         if (sigismember(pending, signo))
//         {
//             cout << "1";
//         }
//         else
//         {
//             cout << "0";
//         }
//     }
//     cout << endl;
// }
// void handle(int sig)
// {
//     cout<<"get a pid"<<getpid()<<endl;
// }
// int main()
// {
//     struct sigaction act;
//     struct sigaction oldact;
//     sigemptyset(&act.sa_mask);
//     act.sa_flags=0;
//     act.sa_handler=handle;
//     sigaction(2,&act,&oldact);//自定义2号信号

//     sigset_t pending;
//     sigset_t block_set;
//     sigset_t old_set;
//     sigemptyset(&block_set);
//     sigemptyset(&old_set);
//     sigaddset(&block_set,2);
//     sigprocmask(SIG_SETMASK,&block_set,&old_set);//将2号信号阻塞
//     int cnt=10;
//     while(true)
//     {
//         sleep(1);
//         sigemptyset(&pending);
//         sigpending(&pending);
//         PrintPending(&pending);
//         cnt--;
//         if(cnt==0)
//         {
//             sigprocmask(SIG_SETMASK,&old_set,&block_set);
//             cout<<"2号信号阻塞清除"<<endl;
//         }
//     }
//     return 0;
// }

// void PrintPending(sigset_t *pending)
// {
//     for (int signo = 31; signo >= 1; signo--)
//     {
//         if (sigismember(pending, signo))
//         {
//             cout << "1";
//         }
//         else
//         {
//             cout << "0";
//         }
//     }
//     cout << endl;
// }
// void handler(int sig)
// {
//     cout << "get a pid:" << getpid() << endl;
// }
// int main()
// {
//     signal(2, handler);
//     // 1、屏蔽2号信号
//     sigset_t block_set, old_set;
//     sigemptyset(&block_set);                      // 初始化
//     sigemptyset(&old_set);                        // 初始化
//     sigaddset(&block_set, 2);                     // 添加2号信号屏蔽
//     sigprocmask(SIG_BLOCK, &block_set, &old_set); // 完成真正的修改
//     int cnt = 10;
//     cout << "get a pid:" << getpid() << endl;
//     while (true)
//     {
//         sleep(1);
//         // 2、获取当前进程的pending进程
//         sigset_t pending;
//         sigpending(&pending);

//         // 3、打印pending信号集
//         PrintPending(&pending);
//         cnt--;
//         // 4、解除对2号信号的屏蔽
//         if (cnt == 0)
//         {
//             cout << "2号信号解除屏蔽" << endl;
//             sigprocmask(SIG_SETMASK, &old_set, &block_set);
//         }
//     }
//     return 0;
// }

// int cnt=0;
// void handler(int sig)
// {
//     cout<<"get a sig:"<<sig<<endl;
//     cout<<cnt<<endl;
// }

// int main()
// {
//     signal(2,handler);
//     alarm(1);
//     while(true)
//     {
//         cnt++;
//     }
//     return 0;
// }

// int main()
// {
//     //对信号的自定义捕捉，我们只要捕捉一次，后续一直有效
//     //1、一直不产生？
//     //2、可不可以对更多的信号进行捕捉？  ---->可以
//     //3、2 SIGINT 默认是什么动作呢？  终止进程
//     //4、2 SIGINT是什么呢？ ctrl+c ----- 给目标进程发送2号信号
//     // signal(2,handler);
//     // signal(3,handler);
//     // signal(4,handler);

//     while(true)
//     {
//         cout<<"hello world,pid:"<<getpid()<<endl;
//         // raise(2);
//         abort();
//         sleep(1);
//     }
//     return 0;
// }