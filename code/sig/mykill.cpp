#include<iostream>
#include<signal.h>
#include<sys/types.h>
#include<unistd.h>
using namespace std;

//./mykill 2 1234
int main(int argc ,char *argv[])
{
    if(argc!=3)
    {
        cout<<"Usage: "<<argv[0]<<"signum pid"<<endl;
        return 1;
    }
    pid_t pid=stoi(argv[2]);
    int sig =stoi(argv[1]);
    kill(pid,sig);
    return 0;
}