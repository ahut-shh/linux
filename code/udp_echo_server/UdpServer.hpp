#pragma once

#include <iostream>
#include<unistd.h>
#include<cstring>
#include<string>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<sys/socket.h>
#include"nocopy.hpp"
#include"Log.hpp"
#include"inetAddr.hpp"
using namespace std;

static const int gsockfd =-1;
static const uint16_t glocalport =8888;
enum
{
    SOCKET_ERROR=1,
    BIND_ERROR
};

class UdpServer:public nocopy
{
public:
    UdpServer(uint16_t localport =glocalport)
    :_sockfd(gsockfd)
    ,_localport(localport)
    ,_isrunning(false)
    {
    }
    ~UdpServer()
    {
        if(_sockfd>gsockfd)::close(_sockfd);
    }

    void InitServer()//初始化服务器
    {
        //1、创建socket文件
        _sockfd =::socket(AF_INET,SOCK_DGRAM,0);
        if(_sockfd<0)
        {
            LOG(FATAL,"socket error\n");
            exit(SOCKET_ERROR);
        }
        LOG(DEBUG,"socket success,_socket:%d\n",_sockfd);
        //2、bind绑定
        struct sockaddr_in local;
        memset(&local,0,sizeof(local));//清零
        local.sin_family=AF_INET;
        local.sin_port = htons(_localport);//端口号,主机序列转网络序列
        // local.sin_addr.s_addr=inet_addr(_localip.c_str());//ip地址,1、4字节，2、需要网络序列的IP
        local.sin_addr.s_addr=INADDR_ANY;//INADDR_ANY 服务器端进行任意IP地址绑定

        int n=::bind(_sockfd,(struct sockaddr*)&local,sizeof(local));
        if(n<0)
        {
            LOG(FATAL,"bind error\n");
            exit(BIND_ERROR);
        }
        LOG(DEBUG,"socket bind success\n");
    }
    void Start()//启动服务器
    {
        _isrunning=true;
        char inbuff[1024];
        while(_isrunning)
        {
            struct sockaddr_in peer;
            socklen_t len =sizeof(peer);
            ssize_t n=recvfrom(_sockfd,inbuff,sizeof(inbuff)-1,0,(struct sockaddr*)&peer,&len);
            if(n>0)
            {
                InetAddr addr(peer);

                inbuff[n]=0;
                cout<<"["<<addr.Ip()<<":"<<addr.Port()<<"]# "<<inbuff<<endl;
                string echo_string ="[udp_server echo]#";
                echo_string+=inbuff;
                
                //返回客户端
                sendto(_sockfd,echo_string.c_str(),echo_string.size(),0,(struct sockaddr*)&peer,len);
            }
        }
    }
private:
    int _sockfd;
    uint16_t _localport;
    // string _localip;
    bool _isrunning;
};