#ifndef __SHM_HPP__
#define __SHM_HPP__

#include <iostream>
#include <sys/types.h>
#include <sys/ipc.h>
#include <string>
#include <cerrno>
#include <cstdio>
#include <sys/shm.h>
#include<unistd.h>
using namespace std;

const string path = "/home/sxh/112_class/shm";
const int proj_id = 0x66;
const int SIZE=4096;

#define gCreatershm 1
#define gUsershm 2
class shm
{
private:
    key_t Getkey()
    {
        key_t key = ftok(_path.c_str(), _proj_id);
        if (key < 0)
        {
            perror("ftok");
        }
        return key;
    }

    int Shmget(key_t key,int size, int flag)
    {
        int shmid = shmget(key, size, flag);
        if (shmid < 0)
        {
            perror("shmget");
        }
        return shmid;
    }

public:
    shm(const string &path, int proj_id, int who) : _path(path), _proj_id(proj_id), _who(who)
    {
        _key=Getkey();
        if(_who==gCreatershm)Createshm();
        else if(_who=gUsershm)Usershm();

        cout<<"shmid:"<<_shmid<<endl;
        cout<<"key:"<<ToHex(_key)<<endl;


        _shmaddr=Attachshm();
    }
    ~shm()
    {
        if(_who==gCreatershm)
        {
            int res=shmctl(_shmid,IPC_RMID,nullptr);
            cout<<"shm delete sucess"<<endl;
        }
        deleteshm(_shmaddr);
    }

    string ToHex(key_t key)
    {
        char buff[128];
        snprintf(buff, sizeof(buff), "0x%x", key);
        return buff;
    }

    bool Createshm()
    {
        if(_who==gCreatershm)
        {
            _shmid=Shmget(_key,SIZE,IPC_CREAT|IPC_EXCL|0666);
            if(_shmid>=0)return true;
            return false;
        }
    }

    bool Usershm()
    {
        if(_who==gUsershm)
        {
            _shmid=Shmget(_key,SIZE,IPC_CREAT|0666);
            if(_shmid>=0)return true;
            return false;
        }
    }

    void *Attachshm()
    {
        void * shmaddr=shmat(_shmid,nullptr,0);
        if(shmaddr==nullptr)
        {
            perror("shmat");
        }
        cout<<"who: "<<_who<<"   attach shm..."<<endl;
        return shmaddr;
    }

    void deleteshm(void * shmaddr)
    {
        if(shmaddr==nullptr)return ;
        int res = shmdt(shmaddr); 
    }

    void * getaddr()
    {
        return _shmaddr;
    }
private:
    key_t _key;
    int _shmid;
    int _who;
    string _path;
    int _proj_id;

    void * _shmaddr;
};

#endif