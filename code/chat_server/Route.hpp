#pragma once

#include <iostream>
#include <string>
#include <unistd.h>
#include <vector>
#include <functional>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "inetAddr.hpp"
#include "Thread.hpp"
#include "ThreadPool.hpp"

using namespace std;

using task_t = function<void()>;

// 路由
class Route
{
private:
    void CheckOnlineUser(InetAddr &who)
    {
        for (auto &it : _online_user)
        {
            if (it == who)
            {
                LOG(DEBUG,"exists\n");
                return;
            }
        }
        _online_user.push_back(who);
    }
    void Offline(InetAddr &who)
    {
        // 迭代器
        auto iter = _online_user.begin();
        while (iter != _online_user.end())
        {
            if (*iter == who)
            {
                _online_user.erase(iter);
                break;
            }
            iter++;
        }
    }
    void ForwardHelper(int sockfd, const string &message)
    {
        // LOG(INFO,"Forwadhelper sucess\n");
        for (auto &user : _online_user)
        {
            struct sockaddr_in peer = user.Addr();
            ::sendto(sockfd, message.c_str(), message.size(), 0, (struct sockaddr *)&peer, sizeof(peer));
        }
    }

public:
    Route()
    {
    }
    void Forward(int sockfd, const string &message, InetAddr &who) // 转发消息，who是谁发的
    {
        // 1、该用户是否在 在线用户列表中，如果在，什么都不做，如果不在，添加到用户列表中
        CheckOnlineUser(who);
        // LOG(INFO,"add sucess\n");
        // 1.1、quit/q  --退出
        if (message == "quit" || message == "q" || message == "QUIT" || message == "Q")
        {
            Offline(who);
        }
        // 2、who一定在列表中-->转发
        //  ForwardHelper(sockfd,message);
        task_t t = std::bind(&Route::ForwardHelper, this, sockfd, message);
        ThreadPool<task_t>::GetInstance()->Equeue(t);
    }
    ~Route()
    {
    }

private:
    vector<InetAddr> _online_user; // 放的是在线的客户端
};