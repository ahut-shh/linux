#include <iostream>
#include <unistd.h>
#include <string>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "Thread.hpp"
using namespace std;
// 客户端
// 一定要知道服务器的IP地址和端口号

int Initserver()
{
    int sockfd = ::socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        cerr << "create socket error" << endl;
    }
    return sockfd;
}

void Recv(int sockfd, const string &name)
{
    while (true)
    {
        struct sockaddr_in temp;
        socklen_t len = sizeof(temp);
        char buff[1024];
        int n = recvfrom(sockfd, buff, sizeof(buff) - 1, 0, (struct sockaddr *)&temp, &len);
        if (n > 0)
        {
            buff[n] = 0;
            cout << buff << endl;
        }
        else
        {
            break;
        }
    }
}
void Sendv(int sockfd, string serverip, uint16_t serverport, const string &name)
{
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(serverport);
    server.sin_addr.s_addr = inet_addr(serverip.c_str());
    string cli_profix = name + " #"; // sender-thread #
    while (true)
    {
        string line;
        cout << cli_profix;
        getline(cin, line);
        // cout<<"get line :"<<line<<endl;

        int n = sendto(sockfd, line.c_str(), line.size(), 0, (struct sockaddr *)&server, sizeof(server)); // 发送消息，要知道发给谁
        if (n <= 0)
            break;
    }
}

// ./udp_server server_ip server_port
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        cerr << "Usage:" << argv[0] << "server-ip server-port" << endl;
        exit(0);
    }
    string serverip = argv[1];
    uint16_t serverport = stoi(argv[2]);

    int sockfd = Initserver();
    Thread recver("recver-thread", std::bind(&Recv, sockfd, std::placeholders::_1));                        // 收消息
    Thread sender("sender-thread", std::bind(&Sendv, sockfd, serverip, serverport, std::placeholders::_1)); // 发消息

    recver.start();
    sender.start();

    recver.join();
    sender.join();

    ::close(sockfd);
}