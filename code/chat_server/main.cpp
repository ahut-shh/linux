#include"UdpServer.hpp"
#include"Route.hpp"

// #include<memory>
int main(int argc,char *argv[])
{
    if(argc!=2)
    {
        cerr<<"Usage: "<<argv[0]<<" lloacl-port"<<endl;
    }
    EnableScreen();
    //智能指针
    // unique_ptr<UdpServer> usvr = make_unique<UdpServer>();//c++14标准
    // uint16_t port=8899;
    // string ip="127.0.0.1";

    uint16_t port=stoi(argv[1]);
    Route messageRoute;
    udpfunc_t f=std::bind(&Route::Forward,&messageRoute,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3);
    UdpServer* usvr = new UdpServer(f,port);
    usvr->InitServer();
    usvr->Start();
    return 0;
}