#pragma once

#include <iostream>
#include <string>
#include <unistd.h>
#include <cstring>
#include <functional>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <pthread.h>
#include "Log.hpp"
#include "inetAddr.hpp"
#include "Thread.hpp"
#include "ThreadPool.hpp"

using namespace std;

const int MAXCOUNT = 1024;
const int gsocket = -1;
enum
{
    SOCKET_ERROR = 1,
    BIND_ERROR
};

using task_t = std::function<void()>;

class TcpServer
{
public:
    TcpServer(uint16_t port) : _port(port), _listensockfd(gsocket), _isrun(false)
    {
    }
    class ThreadDate
    {
    public:
        int _sockfd;
        TcpServer *_self;
        InetAddr _addr;

    public:
        ThreadDate(int sockfd, TcpServer *p, const InetAddr &addr) : _sockfd(sockfd), _self(p), _addr(addr)
        {
        }
    };
    void initserver()
    {
        // 1、创建套接字
        _listensockfd = ::socket(AF_INET, SOCK_STREAM, 0);
        if (_listensockfd < 0)
        {
            LOG(FATAL, "socket error\n");
            exit(SOCKET_ERROR);
        }
        LOG(DEBUG, "socket create sucess\n");
        // 2、绑定
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = INADDR_ANY;

        int n = ::bind(_listensockfd, (struct sockaddr *)&local, sizeof(local));
        if (n < 0)
        {
            LOG(FATAL, "server bind error\n");
            exit(BIND_ERROR);
        }
        LOG(INFO, "binf sucess\n");

        // 3、设置套接字为listen状态
        // 因为tcp是面向连接的，tcp需要未来不断地能够做到获取连接
        int m = listen(_listensockfd, MAXCOUNT);
        if (m < 0)
        {
            LOG(FATAL, "listen error\n");
        }
        LOG(INFO, "listen sucess\n");
    }

    void loop()
    {
        _isrun = true;
        while (_isrun)
        {
            // 获取新连接
            struct sockaddr_in client;
            socklen_t len = sizeof(client);
            int sockfd = ::accept(_listensockfd, (struct sockaddr *)&client, &len);
            if (sockfd < 0)
            {
                LOG(WARNING, "accept error\n");
                continue;
            }
            InetAddr addr(client);
            LOG(INFO, "get a new link,client info :%s\n", addr.Addrstr().c_str());
            // 提供服务
            // version 0
            // Service(sockfd, addr);

            // version 1 多进程版本
            //  pid_t id =fork();
            //  if(id==0)
            //  {
            //      ::close(_listensockfd);
            //      if(fork()>0)exit(0);//子进程退出
            //      Service(sockfd, addr);//孙进程会造成僵尸进程
            //      exit(0);
            //  }
            //  //father
            //  ::close(sockfd);
            //  int n=waitpid(id,nullptr,0);//1、忽略；2、父子孙
            //  if(n>0)
            //  {
            //      LOG(INFO,"wait child sucess\n");
            //  }

            // version 2 多线程版本 ---不能关闭fd，因为是共享的
            // pthread_t tid;
            // ThreadDate *td = new ThreadDate(sockfd, this, addr);
            // pthread_create(&tid, nullptr, Execute, td); // 新线程分离

            // version 3 线程池版本
            // 对应的任务
            task_t aa=std::bind(&TcpServer::Service,this,sockfd,addr);
            // task_t t = std::bind(&TcpServer::Service, this, scokfd, addr);
            ThreadPool<task_t>::GetInstance()->Equeue(aa);
        }

        _isrun = false;
    }
    static void *Execute(void *args)
    {
        pthread_detach(pthread_self());
        ThreadDate *td = static_cast<ThreadDate *>(args);
        td->_self->Service(td->_sockfd, td->_addr);
        delete td;
        return nullptr;
    }
    void Service(int sockfd, InetAddr addr)
    {
        while (true)
        {
            char inbuffer[2048];
            ssize_t n = ::read(sockfd, inbuffer, sizeof(inbuffer) - 1); // 读消息，-1是因为它是系统调用不会做字符串处理
            if (n > 0)
            {
                inbuffer[n] = 0;
                string echo_string = "[server echo]# ";
                echo_string += inbuffer;
                n = ::write(sockfd, echo_string.c_str(), echo_string.size()); // 写消息
            }
            else if (n == 0) // 读到文件结尾，也就是通信结束
            {
                LOG(INFO, "client %s quit\n", addr.Addrstr().c_str());
                break;
            }
            else
            {
                LOG(ERROR, "read server error:%s\n", addr.Addrstr().c_str());
                break;
            }
        }
    }
    ~TcpServer()
    {
        if (_listensockfd != -1)
        {
            ::close(_listensockfd);
        }
    }

private:
    uint16_t _port;
    int _listensockfd;
    bool _isrun;
};