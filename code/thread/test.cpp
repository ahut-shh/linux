#include <iostream>
// #include <pthread.h>
#include <string>
#include <unistd.h>
#include <cstdlib>
#include<vector>
#include"Thread.hpp"
using namespace std;
int num = 1000;

// void *routine(void *args)
// {
//     while (true)
//     {
//         pthread_mutex_lock(&mutex);
//         if (num > 0)
//         {
//             cout << (char *)args << " get a ticket :" << num << endl;
//             num--;
//             pthread_mutex_unlock(&mutex);
//         }
//         else
//         {
//             pthread_mutex_unlock(&mutex);
//             break;
//         }
//     }
//     return nullptr;
// }

void routine(ThreadData *td)
{
    cout<<td->_name<<": mutex address : "<<td->_lock<<endl;
    sleep(1);
    while (true)
    {
        pthread_mutex_lock(td->_lock);
        if (num > 0)
        {
            cout <<td->_name << " get a ticket :" << num << endl;
            num--;
            pthread_mutex_unlock(td->_lock);
        }
        else
        {
            pthread_mutex_unlock(td->_lock);
            break;
        }
    }
    // return nullptr;
}
int main()
{
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex,nullptr);
    
    vector<Thread>threads;
    for(int i=0;i<5;i++)
    {
        string name = "thread-"+to_string(i+1);
        ThreadData *td = new ThreadData(name,&mutex);
        threads.emplace_back(name,routine,td);
    }

    for(auto &thread:threads)
    {
        thread.start();
    }
    for(auto &thread:threads)
    {
        thread.join();
    }

    pthread_mutex_destroy(&mutex);
    // pthread_t tid[5];
    // char *name[5];
    // for (int i = 0; i < 5; i++)
    // {
    //     name[i] = new char[128];
    //     snprintf(name[i], 128, "thread-%d", i + 1);
    //     pthread_create(&tid[i], nullptr, routine, (void *)name[i]);
    // }
    // sleep(1);
    // for (int i = 0; i < 5; i++)
    // {
    //     pthread_join(tid[i], nullptr);
    //     cout << "thread-" << i + 1 << "join sucess ..." << endl;
    //     delete[] name[i];
    // }
}