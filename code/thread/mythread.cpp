#include "Thread.hpp"

void Print(const string &name)
{
    int cnt=1;
    while (true)
    {
        cout<<name<<"is running,cnt:"<<cnt<<endl;
        sleep(1);
    }
}
int main()
{
    Thread t("thread-1", Print);

    t.start();
    cout << t.Name() << " status: " << t.status() << endl;
    sleep(5);
    t.stop();
    cout << t.Name() << " status: " << t.status() << endl;
    sleep(3);
    t.join();
    cout << "join done..." << endl;
    return 0;
}