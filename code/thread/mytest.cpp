#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <cstring>
#include <cerrno>
#include <vector>
using namespace std;

// string ToHex(pthread_t tid)
// {
//     char id[128];
//     snprintf(id,sizeof(id),"0x%x",tid);
//     return id;
// }
// void *Run(void *args)
// {
//     cout << (char *)args << " TID: " << ToHex(pthread_self()) << endl;
//     sleep(5);
// }
// int main()
// {
//     pthread_t tid;
//     pthread_create(&tid, nullptr, Run, (void *)"thread -1");
//     cout << "main thread TID ：" << ToHex(pthread_self())<< endl;
//     sleep(5);
//     return 0;
// }

// const int num = 10;
// class Threaddata
// {
// public:
//     string name;
//     string num;
// };

// void *threadRun(void *args)
// {
//     Threaddata *td = static_cast<Threaddata *>(args);
//     cout << td->name << "-" << td->num << endl;
//     // sleep(1);
//     delete td;
//     // pthread_exit(args);
//     return nullptr;
// }
// int main()
// {
//     vector<pthread_t> q;
//     Threaddata *td[num];
//     for (int i = 0; i < num; i++)
//     {
//         pthread_t tid;
//         td[i] = new Threaddata();
//         td[i]->name = "thread";
//         td[i]->num = to_string(i + 1);
//         pthread_create(&tid, nullptr, threadRun, (void *)td[i]);
//         q.push_back(tid);
//     }
//     // sleep(1);
//     for (auto tid : q)
//     {
//         int n=pthread_detach(tid);
//         cout<<strerror(errno)<<endl;
//         // int n=pthread_cancel(tid);
//         // int n = pthread_join(tid, nullptr);
//         // if (n == 0)
//         // {
//         //     cout << tid << " quit..." << endl;
//         // }
//         // else{
//         //     cout<<strerror(errno)<<endl;
//         // }
//     }
//     return 0;
// }

// class ThreadData
// {
//     public:
//     string name;
//     string num;
//     //other
// };

// void *pthreadRun(void *args)
// {
//     cout << "tid :" << pthread_self() << endl;
//     // ThreadData * td=(ThreadData*)args;
//     ThreadData *td =static_cast<ThreadData*>(args);//static_cast会做安全性检查
//     while (true)
//     {

//         cout << "I am " << td->name <<"-"<<td->num<< endl;
//         sleep(1);
//         break;
//     }
//     delete td;
//     return (void*)111;
// }
// int main()
// {
//     pthread_t tid;
//     // 4、如何全面看待线程函数传参
//     // int a =100;
//     // int n = pthread_create(&tid, nullptr, pthreadRun, (void *)&a);
//     ThreadData *td =new ThreadData();
//     td->name="thread";
//     td->num="1";
//     int n = pthread_create(&tid, nullptr, pthreadRun, (void *)td);
//     if (n != 0)
//     {
//         cout << strerror(errno) << endl;
//         return 1;
//     }

//     // 期望谁最后退出？主线程，还是新线程  -->main thread 如何保证？

//     //5、如何全面看待线程函数返回
//     void *code =nullptr;
//     n = pthread_join(tid, &code);
//     cout<<(uint64_t)code<<endl;
//     if (n == 0)
//     {
//         cout << "main thread wait sucess" << endl;
//     }

//     return 0;
// }