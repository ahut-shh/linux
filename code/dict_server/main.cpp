#include "UdpServer.hpp"
#include "dict.hpp"
// #include<memory>
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cerr << "Usage: " << argv[0] << " lloacl-port" << endl;
    }
    uint16_t port = stoi(argv[1]);
    EnableScreen();
    // 智能指针
    //  unique_ptr<UdpServer> usvr = make_unique<UdpServer>();//c++14标准
    //  uint16_t port=8899;
    //  string ip="127.0.0.1";

    Dict dict("./dict.txt");
    func_t translate = std::bind(&Dict::Translate, &dict, std::placeholders::_1);
    UdpServer *usvr = new UdpServer(translate, port);
    usvr->InitServer();
    usvr->Start();

    return 0;
}