#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <unordered_map>
using namespace std;

const static string sep = ": ";

class Dict
{
public:
    void LoadDict(const string &path)
    {
        ifstream in(path);
        if (!in.is_open())
        {
            LOG(FATAL, "open failed\n");
            exit(1);
        }

        string line;
        while (getline(in, line))
        {
            LOG(DEBUG, "load info %s,sucess\n", line.c_str());
            if (line.empty())
            {
                cout << "empty" << endl;
                continue;
            }
            auto pos = line.find(sep);
            if (pos == std::string::npos)
            {
                cout << "nopos" << endl;
                continue;
            }
            string key = line.substr(0, pos);
            if (key.empty())
            {
                cout << "ket empty" << endl;
                continue;
            }
            string value = line.substr(pos + sep.size());
            if (value.empty())
            {
                cout << "value empty" << endl;
                continue;
            }
            _dict.insert(make_pair(key, value));
        }
        LOG(INFO, "load %s done\n", path.c_str());

        // cout << "_dict size: " << _dict.size() << endl;
        // for (auto it : _dict)
        // {
        //     cout << it.first << ":" << it.second << endl;
        // }
        in.close();
    }

    string Translate(string word)
    {
        if (word.empty())
        {
            cout << "word empty" << endl;
            return "None";
        }
        auto iter = _dict.find(word);
        if (iter == _dict.end())
        {
            cout << "No find" << endl;
            return "None";
        }
        else
            return iter->second;
    }

public:
    Dict(const string &dict_path) : _dict_path(dict_path)
    {
        LoadDict(_dict_path); // 加载数据
    }
    ~Dict()
    {
    }

private:
    unordered_map<string, string> _dict;
    string _dict_path; // 路径
};