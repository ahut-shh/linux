#include <iostream>
#include <unistd.h>
#include <string>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
using namespace std;
// 客户端
// 一定要知道服务器的IP地址和端口号

// ./udp_server server_ip server_port
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        cerr << "Usage:" << argv[0] << "server-ip server-port" << endl;
        exit(0);
    }
    string serverip = argv[1];
    uint16_t serverport = stoi(argv[2]);

    int sockfd = ::socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        cerr << "create socket error" << endl;
    }

    // bind
    // client 的端口号，一般不让用户自己设定，而是让client OS随机选择；怎么选择？什么时候选择？
    // client 需要绑定他自己的ip和端口，但是client 不需要显示绑定他自己的ip和端口
    // client在首次向服务器发送数据的时候，OS会自动给client bind 他自己的IP和端口

    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(serverport);
    server.sin_addr.s_addr = inet_addr(serverip.c_str());
    while (true)
    {
        string line;
        cout << "Please Enter:";
        getline(cin, line);
        // cout<<"get line :"<<line<<endl;

        int n = sendto(sockfd, line.c_str(), line.size(), 0, (struct sockaddr *)&server, sizeof(server)); // 发送消息，要知道发给谁

        if (n > 0)
        {
            struct sockaddr_in temp;
            socklen_t len=sizeof(temp);
            char buff[1024];
            int m=recvfrom(sockfd, buff,sizeof(buff)-1,0,(struct sockaddr *)&temp,&len);
            if(m>0)
            {
                buff[m]=0;
                cout<<buff<<endl;
            }
            else
            {
                break;
            }
        }
        else
        {
            cout<<"sendto error"<<endl;
            break;
        }
    }

    ::close(sockfd);
    return 0;
}