#pragma once

#include <iostream>
#include <string>
#include <pthread.h>
#include <functional>
#include <unistd.h>
#include <cstring>
#include <cerrno>
using namespace std;

// 线程要执行的方法
using func_t = function<void(const string &)>;
// 就类似于 typedef function<void()> func_t ;

// typedef void (*func_t)(ThreadData *td); // 函数指针类型

class Thread
{
public:
    void Excute()
    {
        _isrunning = true;
        _func(_name);
    }

public:
    Thread(const string &name, func_t func) : _name(name), _func(func)
    {
        cout<<"create "<<name<<" done"<<endl;
    }
    ~Thread()
    {
    }
    string status()
    {
        if (_isrunning)
            return "running";
        return "sleep";
    }
    static void *routine(void *args)
    {
        Thread *self = static_cast<Thread *>(args);
        self->Excute();
        return nullptr;
    }
    bool start()
    {
        int n = ::pthread_create(&_tid, nullptr, routine,this); //::强度了使用标准库中的方法
        if (n == 0)
        {
            return true;
        }
        return false;
    }
    void stop()
    {
        if (_isrunning)
        {
            pthread_cancel(_tid);
            _isrunning = false;
        }
    }
    void join()
    {
        if (!_isrunning)
        {
            pthread_join(_tid, nullptr);
        }
    }
    string Name()
    {
        return _name;
    }

private:
    pthread_t _tid;
    string _name;
    bool _isrunning;
    func_t _func; // 线程要执行的回调函数
};
