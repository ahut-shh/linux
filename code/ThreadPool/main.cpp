#include "ThreadPool.hpp"
#include "task.hpp"
int main()
{
    ThreadPool <Task>*tp = new ThreadPool<Task>();
    tp->Init();
    tp->Start();
    int cnt=10;
    while (cnt--)
    {
        sleep(1);
        Task t(1,1);
        tp->Equeue(t);
        sleep(1);
    }
    tp->Stop();
    cout<<"thread pool stop "<<endl;
    sleep(10);
    
    return 0;
}