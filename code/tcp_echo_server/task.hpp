#pragma once

#include <iostream>

using namespace std;

class Task
{
public:
    Task()
    {
    }
    Task(int x, int y) : _x(x), _y(y)
    {
    }
    ~Task()
    {
    }
    int Excute()
    {
        return _x + _y;
    }
    void operator()()
    {
        _result = Excute();
    }

private:
    int _x;
    int _y;
    int _result;
};