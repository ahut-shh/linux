#include"TcpServer.hpp"


// ./server 8888
int main(int argc,char *argv[])
{
    if(argc!=2)
    {
        cerr<<"Usage: "<<argv[0]<<"local-port"<<endl;
        exit(1);
    }
    
    uint16_t port=stoi(argv[1]);
    TcpServer *tsvr = new TcpServer(port);

    tsvr->initserver();
    tsvr->loop();
    return 0;
}