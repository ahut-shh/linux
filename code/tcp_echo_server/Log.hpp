#pragma once

#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <ctime>
#include <stdarg.h>
// #include<stdio.h>
#include<ostream>
#include <fstream>

using namespace std;

#define SCREEN_TYPE 1
#define FILE_TYPE 2
const string glogfile ="./log.txt";

// 日志等级
enum
{
    DEBUG = 1,
    INFO,
    WARNING,
    ERROR,
    FATAL
};

string levelTo_string(int level)
{
    switch (level)
    {
    case DEBUG:
        return "DEBUG";
    case INFO:
        return "INFO";
    case WARNING:
        return "WARNING";
    case ERROR:
        return "ERROR";
    case FATAL:
        return "FATAL";

    default:
        return "UNKNOWN";
    }
}

string gettime()
{
    time_t now = time(nullptr); // now就是时间戳
    struct tm *curr_time = localtime(&now);
    char buff[128];
    snprintf(buff, sizeof(buff), "%d-%02d-%02d : %02d-%02d-%02d",
             curr_time->tm_year + 1900,
             curr_time->tm_mon + 1,
             curr_time->tm_mday,
             curr_time->tm_hour,
             curr_time->tm_min,
             curr_time->tm_sec);
    return buff;
}

class Logmessage
{
public:
    string _level;
    pid_t _pid;
    string _filename;
    int _filenumber;
    string _curr_time;
    string _message_info;
};

class Log
{
private:
    void FlushLogScreen(Logmessage &lg)
    {
        printf("[%s][%d][%s][%d] %s",lg._level.c_str(),lg._pid,lg._filename.c_str(),lg._filenumber,lg._message_info.c_str());
    }
    void FlushLogFile(Logmessage &lg)
    {
        ofstream out(_logfile.c_str());
        if(!out.is_open())return;
        char buff[2048];
        snprintf(buff,sizeof(buff),"[%s][%d][%s][%d] %s",lg._level.c_str(),lg._pid,lg._filename.c_str(),lg._filenumber,lg._message_info.c_str());
        out.write(buff,strlen(buff));
        out.close();
    }

public:
    Log(const string &logfile = glogfile) : _type(SCREEN_TYPE), _logfile(logfile)
    {
    }
    ~Log()
    {
    }
    void Enable(int type)
    {
        _type = type;
    }
    void FlushLog(Logmessage &lg)
    {
        switch (_type)
        {
        case SCREEN_TYPE:
            FlushLogScreen(lg);
            break;
        case FILE_TYPE:
            FlushLogFile(lg);
            break;
        }
    }
    void logMessage(string filename, int filenumber, int level, const char *format, ...)
    {
        Logmessage lg;
        lg._level = levelTo_string(level);
        lg._pid = getpid();
        lg._filename = filename;
        lg._filenumber = filenumber;
        lg._curr_time = gettime();

        va_list ap;
        va_start(ap, format);
        char log_info[1024];
        vsnprintf(log_info, sizeof(log_info), format, ap);
        va_end(ap);
        lg._message_info = log_info;

        FlushLog(lg);
    }

private:
    int _type;
    string _logfile;
};

Log lg;
#define LOG(Level, Format, ...)do{lg.logMessage(__FILE__, __LINE__, Level, Format, ##__VA_ARGS__); } while (0)
#define EnableScreen()do{lg.Enable(SCREEN_TYPE);} while (0)
#define EnableFILE()do{lg.Enable(FILE_TYPE); } while (0)