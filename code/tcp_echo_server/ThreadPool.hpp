#pragma once

#include <iostream>
#include <unistd.h>
#include <string>
#include <vector>
#include <queue>
#include "Thread.hpp"
#include"task.hpp"
#include"LockGuard.hpp"

using namespace std;
const int defaultnum = 5;
template <typename T>
class ThreadPool
{
private:
    void WakeUpAll()
    {
        pthread_cond_broadcast(&_cond);
    }
    void Lock()
    {
        pthread_mutex_lock(&_mutex);
    }
    void Unlock()
    {
        pthread_mutex_unlock(&_mutex);
    }
    void WakeUp()
    {
        pthread_cond_signal(&_cond);
    }
    bool isEmpty()
    {
        return _task_queue.empty();
    }
    void HandlerTask(const string &name)
    {
        while (true)
        {
            // 取任务
            Lock();
            while (isEmpty() && _isrunning)
            {
                // 休眠
                _sleep_num++;
                pthread_cond_wait(&_cond, &_mutex);
                _sleep_num--;
            }
            if (isEmpty() && !_isrunning)
            {
                cout << name << "quit..." << endl;
                Unlock();
                break;
            }
            // 有任务
            T t = _task_queue.front();
            _task_queue.pop();
            Unlock();

            // 处理任务
            t();

            cout << name << "任务处理完" << endl;
        }
    }

public:
    ThreadPool(int thread_num = defaultnum) : _thread_num(thread_num), _isrunning(false), _sleep_num(0)
    {
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_cond, nullptr);
    }
    ~ThreadPool()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_cond);
    }
    void Init()
    {
        func_t func = bind(&ThreadPool::HandlerTask, this, std::placeholders::_1);
        // 创建线程
        for (int i = 0; i < _thread_num; i++)
        {
            string name = "thread-" + to_string(i + 1);
            _threads.emplace_back(name, func);
        }
    }
    void Start()
    {
        _isrunning = true;
        for (auto &thread : _threads)
        {
            thread.start();
        }
    }
    void Stop()
    {
        Lock();
        _isrunning = false;
        WakeUpAll();
        Unlock();
    }
    // 如果是多线程获取单例呢？
    static ThreadPool<T> *GetInstance()
    {
        if (_tp == nullptr)
        {
            LockGuard lockguard(&_sig_mutex);
            if (_tp == nullptr)
            {
                LOG(INFO, "create threadpool\n");
                // thread-1 thread-2 thread-3....
                _tp = new ThreadPool();
                _tp->Init();
                _tp->Start();
            }
            else
            {
                LOG(INFO, "get threadpool\n");
            }
        }
        return _tp;
    }
    void Equeue(const T &in)
    {
        LOG(DEBUG, "push sucess\n");
        Lock();
        if (_isrunning)
        {
            // 生产任务
            _task_queue.push(in);
            // 唤醒线程
            if (_sleep_num > 0)
            {
                WakeUp();
            }
        }
        Unlock();
    }

private:
    int _thread_num;
    vector<Thread> _threads; // 线程
    queue<T> _task_queue;    // 任务，共享资源
    bool _isrunning;
    int _sleep_num; // 休眠的个数

    pthread_mutex_t _mutex;
    pthread_cond_t _cond;

    static ThreadPool<T> *_tp;
    static pthread_mutex_t _sig_mutex;
};
template <typename T>
ThreadPool<T> *ThreadPool<T>::_tp = nullptr;
template <typename T>
pthread_mutex_t ThreadPool<T>::_sig_mutex = PTHREAD_MUTEX_INITIALIZER;