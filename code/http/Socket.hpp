#pragma once

#include <iostream>
#include <string>
#include <unistd.h>
#include <cstring>
#include <functional>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <pthread.h>
#include "Log.hpp"
#include "inetAddr.hpp"
#include <memory>

using namespace std;

const static int gblcklog = 8;

enum
{
    SOCKET_ERROR = 1,
    BIND_ERROR,
    LISTEN_ERR
};

class Socket;

using SockSptr = std::shared_ptr<Socket>;

class Socket
{
public:
    // 模板方法模式
    virtual void CreateSocketOrDie() = 0;
    virtual void CreateBindOrDie(uint16_t port) = 0;
    virtual void CreateListenOrDie(int backlog = gblcklog) = 0;
    virtual SockSptr Accepter(InetAddr *cliaddr) = 0;
    virtual bool Connector(const string &peerip, uint16_t peerport) = 0;

    virtual int Sockfd() = 0;
    virtual void Close() = 0;

    virtual ssize_t Recv(std::string *out) = 0;
    virtual ssize_t Send(const std::string &in) = 0;

public:
    void BuildListenSocket(uint16_t port)
    {
        CreateSocketOrDie();
        CreateBindOrDie(port);
        CreateListenOrDie();
    }
    void BuildClientSocket(const string &peerip, uint16_t peerport)
    {
        CreateSocketOrDie();
        Connector(peerip, peerport);
    }
};




class TcpSocket : public Socket
{
public:
    TcpSocket()
    {
    }
    ~TcpSocket()
    {
        if (_sockfd > 0)
        {
            ::close(_sockfd);
        }
    }
    TcpSocket(int sockfd) : _sockfd(sockfd)
    {
    }
    void CreateSocketOrDie() override
    {
        _sockfd = ::socket(AF_INET, SOCK_STREAM, 0);
        if (_sockfd < 0)
        {
            LOG(FATAL, "socket error\n");
            exit(SOCKET_ERROR);
        }
        LOG(DEBUG, "socket create sucess\n");
    }
    void CreateBindOrDie(uint16_t port) override
    {
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(port);
        local.sin_addr.s_addr = INADDR_ANY;

        int n = ::bind(_sockfd, (struct sockaddr *)&local, sizeof(local));
        if (n < 0)
        {
            LOG(FATAL, "server bind error\n");
            exit(BIND_ERROR);
        }
        LOG(INFO, "bind sucess\n");
    }
    void CreateListenOrDie(int backlog) override
    {
        if (::listen(_sockfd, gblcklog) < 0)
        {
            LOG(FATAL, "listen error\n");
            exit(LISTEN_ERR);
        }
    }
    SockSptr Accepter(InetAddr *cliaddr) override
    {
        struct sockaddr_in client;
        socklen_t len = sizeof(client);
        int sockfd = ::accept(_sockfd, (struct sockaddr *)&client, &len);
        if (sockfd < 0)
        {
            LOG(WARNING, "accept error\n");
            return nullptr;
        }
        *cliaddr = InetAddr(client);
        LOG(INFO, "get a new link,client info :%s\n", cliaddr->Addrstr().c_str());

        return std::make_shared<TcpSocket>(sockfd);
    }

    bool Connector(const string &peerip, uint16_t peerport) override
    {
        struct sockaddr_in server;
        memset(&server, 0, sizeof(server));
        server.sin_family = AF_INET;
        server.sin_port = htons(peerport);
        ::inet_pton(AF_INET, peerip.c_str(), &server.sin_addr);

        // connect，连接服务器
        int n = ::connect(_sockfd, (struct sockaddr *)&server, sizeof(server));
        if (n < 0)
        {
            cerr << "connect error" << endl;
            exit(2);
            return false;
        }
        return true;
    }

    int Sockfd()
    {
        return _sockfd;
    }
    void Close()
    {
        if (_sockfd > 0)
        {
            ::close(_sockfd);
        }
    }
    ssize_t Recv(std::string *out) override
    {
        char inbuffer[4096];
        ssize_t n = ::recv(_sockfd, inbuffer, sizeof(inbuffer) - 1, 0);
        if (n > 0)
        {
            inbuffer[n] = 0;
            *out += inbuffer;
        }
        return n;
    }
    ssize_t Send(const std::string &in) override
    {
        return ::send(_sockfd, in.c_str(), in.size(), 0);
    }

private:
    int _sockfd;
};