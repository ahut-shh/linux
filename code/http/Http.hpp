#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include<functional>
#include <unordered_map>

const static string base_sep = "\r\n";
const static string line_sep = ": ";
const static string prefixpath = "wwwroot"; // web根目录
const static string homepage = "index.html";
const static string httpversion = "HTTP/1.0";
const static string spacesep = " ";
const static string suffixsep = ".";
const static string html_404 = "404.html";
const static string arg_sep = "?";

class HttpResquest // 请求
{
private:
    string GetLine(string &reqstr)
    {
        auto pos = reqstr.find(base_sep);
        if (pos == std::string::npos)
        {
            return string();
        }
        string line = reqstr.substr(0, pos);
        reqstr.erase(0, line.size() + base_sep.size());
        return line.empty() ? base_sep : line;
    }
    void ParseReqLine()
    {
        stringstream ss(_req_line);        // 字符串转化成为字符串流
        ss >> _method >> _url >> _version; // /a/b/c.html or /login?username=shhhhhh&userpasswd=1234567890 HTTP/1.1

        if (strcasecmp(_method.c_str(), "GET") == 0) // 比较两个字符串，忽略大小写
        {
            auto pos = _url.find(arg_sep);
            if (pos != std::string::npos)
            {
                _body_test = _url.substr(pos + arg_sep.size());
                _url.resize(pos);
            }
        }

        _path += _url;
        if (_path[_path.size() - 1] == '/')
        {
            _path += homepage;
        }

        auto pos = _path.rfind(suffixsep);
        if (pos != string::npos)
        {
            _suffix = _path.substr(pos);
        }
        else
        {
            _suffix = ".default";
        }
    }

    void ParseReqHeader()
    {
        for (auto &header : _req_headers)
        {
            auto pos = header.find(line_sep);
            if (pos == std::string::npos)
                continue;
            string k = header.substr(0, pos);
            string v = header.substr(pos + line_sep.size());
            if (k.empty() || v.empty())
                continue;

            _headers_kv.insert(make_pair(k, v));
        }
    }

public:
    HttpResquest() : _blank_line(base_sep), _path(prefixpath)
    {
    }
    // 反序列化
    void Deserialize(string &reqstr)
    {
        // 基本的反序列化
        _req_line = GetLine(reqstr);
        string header;
        do
        {
            header = GetLine(reqstr);
            if (header.empty())
                break;
            else if (header == base_sep)
                break;
            _req_headers.push_back(header);
        } while (true);

        if (!reqstr.empty())
        {
            _body_test = reqstr;
        }
        // 进一步反序列化
        ParseReqLine();
        ParseReqHeader();
    }

    string Url()
    {
        LOG(DEBUG, "Client want %s\n", _url.c_str());
        return _url;
    }

    string Path()
    {
        LOG(DEBUG, "Client want path %s\n", _path.c_str());
        return _path;
    }

    string Suffix()
    {
        return _suffix;
    }

    string Method()
    {
        LOG(DEBUG, "Client resquest method is %s\n", _method.c_str());
        return _method;
    }

    string GetRequestBody()
    {
        LOG(DEBUG,"Client request method is %s,args:%s\n",_method.c_str(),_body_test.c_str());
        return _body_test;
    }
    void Print()
    {
        cout << "---------------------------" << endl;
        cout << "###" << _req_line << endl;
        for (auto &header : _req_headers)
        {
            cout << "@@@" << header << endl;
        }
        cout << "***" << _blank_line;
        cout << ">>>" << _body_test << endl;
        cout << "Method: " << _method << endl;
        cout << "Url: " << _url << endl;
        cout << "Version: " << _version << endl;

        for (auto &head_kv : _headers_kv)
        {
            cout << ")))" << head_kv.first << "->" << head_kv.second << endl;
        }
    }
    ~HttpResquest() {}

private:
    // 基本的httpresquest的格式
    string _req_line;            // 请求行
    vector<string> _req_headers; // 请求报头
    string _blank_line;          // 空行
    string _body_test;           // 正文

    // 更具体的属性字段，需要进一步的反序列化
    string _method;
    string _url;
    string _path;
    string _version;
    string _suffix; // 资源后缀
    unordered_map<string, string> _headers_kv;
};

class HttpResponse // 响应
{
public:
    HttpResponse() : _version(httpversion), _blank_line(base_sep)
    {
    }
    void AddCode(int code, const string &desc)
    {
        statuc_code = code;
        _desc = desc;
    }

    void AddHeader(const string &k, const string &v)
    {
        _headers_kv[k] = v;
    }

    void AddBodyText(const string body_test)
    {
        _resp_body_test = body_test;
    }

    string Seserialize()
    {
        // 1、构建报头
        _status_line = _version + spacesep + to_string(statuc_code) + spacesep + _desc + base_sep;
        // 2、构建响应报头
        for (auto &header : _headers_kv)
        {
            string header_line = header.first + line_sep + header.second + base_sep;
            _resp_headers.push_back(header_line);
        }

        // 3、正文

        // 4、正式序列化
        string responsestr = _status_line;
        for (auto &line : _resp_headers)
        {
            responsestr += line;
        }
        responsestr += _blank_line;
        responsestr += _resp_body_test;

        return responsestr;
    }

    ~HttpResponse() {}

private:
    // httpresponse base属性
    string _version; // 版本
    int statuc_code; // 状态码
    string _desc;
    unordered_map<string, string> _headers_kv;
    //  基本的httpresquest的格式
    string _status_line;          // 状态行
    vector<string> _resp_headers; // 响应报头
    string _blank_line;           // 空行
    string _resp_body_test;       // 正文
};

using func_t =std::function<HttpResponse(HttpResquest &)>;

class HttpServer
{
private:
    string GetFileContent(const string &path) // 读取文件内容
    {
        ifstream in(path, std::ios::binary);
        if (!in.is_open())
            return string();

        in.seekg(0, in.end);
        int filesize = in.tellg(); // 告知我，你的偏移量是多少
        in.seekg(0, in.beg);

        string content;
        content.resize(filesize);

        in.read((char *)content.c_str(), filesize);

        in.close();

        return content;
    }

public:
    HttpServer()
    {
        _mime_type.insert(make_pair(".html", "text/html"));
        _mime_type.insert(make_pair(".jpg", "image/jpg"));
        _mime_type.insert(make_pair(".png", "image/png"));
        _mime_type.insert(make_pair(".default", "text/html"));

        _code_to_desc.insert(make_pair(100, "Continue"));
        _code_to_desc.insert(make_pair(200, "OK"));
        _code_to_desc.insert(make_pair(301, "Moved Permanently"));
        _code_to_desc.insert(make_pair(302, "Found"));
        _code_to_desc.insert(make_pair(404, "Not Found"));
    }
    // #define TEST
    string HandlerHttpRequest(string &reqstr)
    {
#ifdef TEST
        cout << "---------------------------" << endl;

        cout << reqstr;

        string responsestr = "HTTP/1.1 200 ok\r\n";
        responsestr += "Content-Typr: text/html\r\n";
        responsestr += "\r\n";
        responsestr += "<html><h1>hello Linux!</h1><html>";
        return responsestr;
#else
        cout << "---------------------------" << endl;
        cout << reqstr;
        cout << "---------------------------" << endl;

        HttpResquest req;
        HttpResponse resp;

        req.Deserialize(reqstr); // 反序列化
        req.Method();

        if (req.Path() == "wwwroot/redir")
        {
            //处理重定向
            string redir_path = "https://www.qq.com";
            resp.AddCode(301, _code_to_desc[301]);
            resp.AddHeader("Location", redir_path);
        }
        else if(!req.GetRequestBody().empty())
        {
            if(IsService(req.Path()))
            {
                resp=_service_list[req.Path()](req);
            }
        }
        else
        {
            //最基本的上层处理，处理静态资源
            string content = GetFileContent(req.Path());

            if (content.empty())
            {
                content = GetFileContent("wwwroot/404.html");
                resp.AddCode(404, _code_to_desc[404]);
                resp.AddHeader("Content-Length", to_string(content.size()));
                resp.AddHeader("Content-Type", _mime_type[".html"]);
            } // TODO

            resp.AddCode(200, _code_to_desc[200]);
            resp.AddHeader("Content-Length", to_string(content.size()));
            resp.AddHeader("Content-Type", _mime_type[req.Suffix()]);
            resp.AddHeader("Set-Cookie","username=shhh");
            resp.AddHeader("Set-Cookie","passwd=12345");
            resp.AddBodyText(content);
        }

        return resp.Seserialize();
#endif
    }

    void InsertService(const string servicename,func_t f)
    {
        string s =prefixpath+servicename;
        _service_list[s]=f;
    }

    bool IsService(const string &servicename)//判断服务是否存在
    {
        auto iter=_service_list.find(servicename);
        if(iter==_service_list.end())return false;
        return true;
    }

    ~HttpServer() {}

private:
    unordered_map<string, string> _mime_type;
    unordered_map<int, string> _code_to_desc; // 状态码描述
    std::unordered_map<std::string,func_t> _service_list;//服务列表
};

// class HttpServer
// {
// public:
//     HttpServer() {}
//     string HandlerHttpRequest(string &reqstr)
//     {
//         cout << "---------------------------" << endl;
//         cout << reqstr;
//         string responsestr = "HTTP/1.1 200 ok\r\n";
//         responsestr += "Content-Typr: text/html\r\n";
//         responsestr += "\r\n";
//         responsestr += "<html><h1>hello Linux!</h1><html>";
//         return responsestr;
//     }
//     ~HttpServer() {}
// };