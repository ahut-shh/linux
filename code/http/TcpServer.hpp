#pragma once

#include <iostream>
#include <functional>
#include "Socket.hpp"
#include "Log.hpp"
#include "inetAddr.hpp"

using namespace std;

static const int gport = 8888;


using service_t = std::function<string(string &resqueststr)>;

class TcpServer
{
public:
    TcpServer(service_t service, int port = gport) : _port(port), _listensockfd(std::make_shared<TcpSocket>()), _isrun(false), _service(service)
    {
        _listensockfd->BuildListenSocket(_port);
    }
    class ThreadDate
    {
    public:
        SockSptr _sockfd;
        TcpServer *_self;
        InetAddr _addr;

    public:
        ThreadDate(SockSptr sockfd, TcpServer *self, const InetAddr &addr) : _sockfd(sockfd), _self(self), _addr(addr)
        {
        }
    };

    void loop()
    {
        _isrun = true;
        while (_isrun)
        {
            // 获取新连接
            InetAddr client;
            SockSptr newsock = _listensockfd->Accepter(&client);
            if (newsock == nullptr)
                continue;
            LOG(INFO, "get a new link, client info : %s\n", client.Addrstr().c_str());

            // version 2 多线程版本 ---不能关闭fd，因为是共享的
            pthread_t tid;
            ThreadDate *td = new ThreadDate(newsock, this, client);
            pthread_create(&tid, nullptr, Execute, td); // 新线程分离
        }

        _isrun = false;
    }
    static void *Execute(void *args)
    {
        pthread_detach(pthread_self());
        ThreadDate *td = static_cast<ThreadDate *>(args);

        std::string requeststr;
        ssize_t n = td->_sockfd->Recv(&requeststr);
        if (n > 0)
        {
            string responsestr = td->_self->_service(requeststr);
            td->_sockfd->Send(responsestr);
        }

        td->_sockfd->Close();
        delete td;
        return nullptr;
    }

    ~TcpServer()
    {
    }

private:
    uint16_t _port;
    SockSptr _listensockfd;
    bool _isrun;
    service_t _service;
};