#include "TcpServer.hpp"
#include "Http.hpp"


HttpResponse Login(HttpResquest& req)
{
    HttpResponse resp;
    req.GetRequestBody();
    resp.AddCode(200,"OK");
    resp.AddBodyText("<html><h1>result</h1></html>");
    return resp;
}

// ./tcpserver 8888
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " local-port" << std::endl;
        exit(0);
    }
    uint16_t port = std::stoi(argv[1]);

    HttpServer hserver;

    //想要支持登录功能：
    hserver.InsertService("/login",Login);

    std::unique_ptr<TcpServer> tsvr = std::make_unique<TcpServer>(
        std::bind(&HttpServer::HandlerHttpRequest, &hserver, std::placeholders::_1), 
        port
    );
    tsvr->loop();

    return 0;
}