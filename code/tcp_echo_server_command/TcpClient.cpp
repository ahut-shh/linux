#include <iostream>
#include <string>
#include<stdio.h>
#include <unistd.h>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "Log.hpp"
#include "inetAddr.hpp"
using namespace std;

//./tcpclient server-ip,server-port
int main(int argc,char* argv[])
{
    if(argc!=3)
    {
        cerr<<"Usage:"<<argv[0]<<"server-ip server-port"<<endl;
        exit(0);
    }
    string serverip=argv[1];
    uint16_t serverport =stoi(argv[2]);
    //1、创建套接字
    int _sockfd = ::socket(AF_INET,SOCK_STREAM,0);
    if(_sockfd<0)
    {
        LOG(ERROR,"socket create error\n");
        exit(1);
    }
    //2、绑定
    struct sockaddr_in server;
    memset(&server,0,sizeof(server));
    server.sin_family=AF_INET;
    server.sin_port=htons(serverport);
    ::inet_pton(AF_INET,serverip.c_str(),&server.sin_addr);

    //connect，连接服务器
    int n=::connect(_sockfd,(struct sockaddr*)&server,sizeof(server));
    if(n<0)
    {
        cerr<<"connect error"<<endl;
        exit(2);
    }

    while(true)
    {
        string message;
        cout<<"Enter #";
        getline(std::cin,message);
        write(_sockfd,message.c_str(),message.size());

        char echo_buff[2048];
        n=read(_sockfd,echo_buff,sizeof(echo_buff));
        if(n>0)
        {
            echo_buff[n]=0;
            cout<<echo_buff<<endl;
        }
        else{
            LOG(ERROR,"read client error\n");
            break;
        }
    }

    ::close(_sockfd);
    return 0;
}