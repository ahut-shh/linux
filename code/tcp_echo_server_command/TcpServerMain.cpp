#include "TcpServer.hpp"
#include "command.hpp"

// ./server 8888
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cerr << "Usage: " << argv[0] << "local-port" << endl;
        exit(1);
    }

    uint16_t port = stoi(argv[1]);
    Command cmservice;
    TcpServer *tsvr = new TcpServer(std::bind(&Command::HandlerCommand,&cmservice,std::placeholders::_1,std::placeholders::_2),port);

    tsvr->initserver();
    tsvr->loop();
    return 0;
}