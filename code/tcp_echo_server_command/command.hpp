#pragma once

#include <iostream>
#include <string>
#include <unistd.h>
#include <cstring>
#include<stdio.h>
#include <functional>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <pthread.h>
#include "Log.hpp"
#include "inetAddr.hpp"

using namespace std;

class Command
{
public:
    Command()
    {
    }
    ~Command()
    {
    }
    string Excute(const string &cmdstr)
    {
        FILE *fp=popen(cmdstr.c_str(),"r");
        string result;
        if(fp)
        {
            char line[1024];
            while(fgets(line,sizeof(line),fp))//读文件
            {
                result+=line;
            }
            return result;
        }
        else{
            return "execute error";
        }
    }
    void HandlerCommand(int sockfd, InetAddr addr)
    {
        while (true)
        {
            char inbuffer[2048];
            ssize_t n = ::recv(sockfd, inbuffer, sizeof(inbuffer) - 1,0); // 读消息，-1是因为它是系统调用不会做字符串处理
            if (n > 0)
            {
                inbuffer[n] = 0;
                string result = Excute(inbuffer);
                n = ::send(sockfd, result.c_str(), result.size(),0); // 写消息
            }
            else if (n == 0) // 读到文件结尾，也就是通信结束
            {
                LOG(INFO, "client %s quit\n", addr.Addrstr().c_str());
                break;
            }
            else
            {
                LOG(ERROR, "read server error:%s\n", addr.Addrstr().c_str());
                break;
            }
        }
    }

};