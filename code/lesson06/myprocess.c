#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<string.h>

int main()
{
	//extern char **environ;
    //
	//int i;
	//for(i=0;environ[i];i++)
	//{
	//	printf("env[%d]->%s\n",i,environ[i]);
	//}



	int val=1000;
	printf("I am father process,pid:%d, ppid:%d\n",getpid(),getppid());
	sleep(5);
	pid_t id =fork();
	if(id==0)
	{
		while(1)
		{
			val=200;
			printf("I am child process,pid:%d, ppid:%d, val:%d,   &val:%p\n",getpid(),getppid(),val,&val);
			sleep(1);
		}
	}
	else
	{
		while(1)
		{
			printf("I am father process,pid:%d, ppid:%d, val:%d,  &val:%p\n",getpid(),getppid(),val,&val);
			sleep(1);
		}
	}
}


//int main(int argc,char *argv[])
//{
//	//int i;
//	//for( i=0;i<argc;i++)
//	//{
//	//	printf("argv[%d]=%s\n",i,argv[i]);
//	//}
//
//
//
//	//while(1)
//	//{
//	//	printf("I am a process,pid:%d\n",getpid());
//	//	sleep(1);
//	//}
//	return 0;
//}
