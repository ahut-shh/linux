#include<iostream>
#include<unistd.h>
#include<cerrno>
#include<cstring>
#include<string>
#include<sys/wait.h>
#include<sys/types.h>
using namespace std;

const int size=1024;

void childwrite(int wfd)
{
    string message="child process";//发送给父进程的信息
        while(true)
        {
            write(wfd,message.c_str(),message.size());//写入管道时，没有写入\0，没有必要写入;
            sleep(1);
        }
}

void   father_read(int rfd)
{
    char inbuffer[size];
    while(true)
    {
        sleep(500);
        ssize_t n=read(rfd,inbuffer,sizeof(inbuffer)-1);
         if(n>0)
         {
            inbuffer[n]=0;
            cout<<"father get message:"<<inbuffer<<endl;
         }
        
    
    }
   
}

int main()
{
    //1、创建管道
    int pipfd[2];
    int n =pipe(pipfd);//输出型参数，rfd,wrd
    if(n!=0)
    {
        cerr<<"errno:"<<errno<<":"<<"errstring:"<<strerror(errno)<<endl;
    }
    //管道创建成功
    cout<<"pipfd[0]:"<<pipfd[0]<<" pipfd[1]"<<pipfd[1]<<endl;

    //2、创建子进程
    pid_t id=fork();
    //3、关闭不需要的fd
    if(id==0)
    {
        //子进程,关闭读操作
        close(pipfd[0]);
        //进行通信
        childwrite(pipfd[1]);
        close(pipfd[1]);
        exit(0);
    }
    //父进程,关闭写操作
    close(pipfd[1]);
    father_read(pipfd[0]);

    close(pipfd[0]);
    waitpid(id,nullptr,0);
    return 0;
}