#pragma once

#include<iostream>
#include<pthread.h>
#include<semaphore.h>
#include<vector>
#include<string>
#include<unistd.h>
#include<sys/types.h>

using namespace std;
const int  defaultcp =5;
template<typename T>
class Ringqueue
{
private:
    void P(sem_t &s)//申请信号量,--
    {
        sem_wait(&s);
    }
    void V(sem_t &s)//释放资源，++
    {
        sem_post(&s);
    }
public:
    Ringqueue(int max_cp = defaultcp):_max_cp(max_cp),_ringqueue(max_cp),_c_step(0),_p_step(0)
    {
        sem_init(&_data_sem,0,0);
        sem_init(&_space_sem,0,max_cp);

    }
    ~Ringqueue()
    {
        sem_destroy(&_data_sem);
        sem_destroy(&_space_sem);
    }

    void Push(const T &in)//生产
    {
        P(_space_sem);
        _ringqueue[_p_step]=in;
        _p_step++;
        _p_step%=_max_cp;
        V(_data_sem);
    }
    void Pop(T *out)//消费
    {
        P(_data_sem);
        *out=_ringqueue[_c_step];
        _c_step++;
        _c_step%=_max_cp;
        V(_space_sem);
    }

private:
    vector<T>_ringqueue;
    int _p_step;
    int _c_step;
    int _max_cp;

    sem_t _data_sem;
    sem_t _space_sem;
};