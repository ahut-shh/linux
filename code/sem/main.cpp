#include "RingQueue.hpp"
#include "task.hpp"
#include <ctime>

void *Cunsumer(void *args)
{
    Ringqueue<Task> *rq = static_cast<Ringqueue<Task> *>(args);
    while (true)
    {

        // 1、消费
        Task t;
        rq->Pop(&t);
        // 2、处理数据
        cout << "Cunsumer->" << t.Excute() << endl;
    }
}

void *Productor(void *args)
{
    Ringqueue<Task> *rq = static_cast<Ringqueue<Task> *>(args);
    while (true)
    {
        sleep(1);
        // 1、构造数据
        // int data = rand() % 10 + 1;
        int x=rand()%10+1;
        int y= rand()%10+1;
        Task t(x,y);
        // 2、生产
        rq->Push(t);
        cout << "Productor->" << t.Excute()<< endl;
    }
}

int main()
{
    srand(time(nullptr) ^ getpid());
    Ringqueue<Task> *rq = new Ringqueue<Task>();
    pthread_t c, p;
    pthread_create(&c, nullptr, Cunsumer, rq);
    pthread_create(&p, nullptr, Productor, rq);

    pthread_join(c, nullptr);
    pthread_join(p, nullptr);

    return 0;
}