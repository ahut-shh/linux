#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<stdlib.h>
#include<sys/wait.h>

int main()
{

	printf("I am father process, pid:%d,ppid:%d\n",getpid(),getppid());
	pid_t id = fork();
	if(id==0)
	{
		int cnt=5;
		while(cnt)
		{
			printf("I am child process,pid:%d,ppid:%d,cnt:%d\n",getpid(),getppid(),cnt);
			cnt--;
			sleep(1);
		}
		printf("child quit...\n");
		exit(0);
	}
	sleep(10);
	//pid_t rid =wait(NULL);
	
	int status=0;
	pid_t rid = waitpid(-1,&status,WNOHANG);

	
	if(rid>0)
	{
		if(WIFEXITED(status))
		{
			printf("child quit sucess,child exit code:%d\n",WEXITSTATUS(status));
		}
		else{
			printf("child quit unnormal!\n");
		}
		printf("wait sucess,rid:%d\n",rid);

	}
	else
	{
		printf("wait failed !\n");
	}
	sleep(3);
	//printf("father quit,status:%d,child quit code:%d,child quit signal:%d\n",status,(status>>8)&0xFF,status&0x7F);

	//printf("hello");
	//sleep(2);
	//_exit(1);
	return 0;
}


//int main()
//{
//	printf("pid:%d\n",getpid());
//	int errcode;
//	exit(10);
//	for(errcode=0;errcode<=255;errcode++)
//	{
//		printf("%d:%s\n",errcode,strerror(errcode));
//	}
//
//	sleep(2);
//	return 0;
//
//	//printf("I am fatherprocess, pid:%d,ppid:%d\n",getpid(),getppid());
//	//return 100;
//}
