#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<unistd.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<ctype.h>
#include <sys/stat.h>
 #include <fcntl.h>


# define ZERO '\0'
# define SIZE 512
# define NUM 32
# define SEP " "
# define SkipPath(p) do{p+=(strlen(p)-1);while(*p!='/')p--;}while(0)
#define	SkipSpace(cmd,pos)do{while(1){if(isspace(cmd[pos]))pos++;else break;}}while(0)
char cwd[SIZE*2];
char * gArgv[NUM];
int lastcode =0;




// "ls -a -l -n > myfile.txt"
#define None_Redir 0
#define IN_Redir   1
#define OUT_Redir  2
#define App_Redir  3

int redir_type = None_Redir;
char *filename =NULL;


const char *Getusername()
{
	const char *name=getenv("USER");
	if(name==NULL)return "None";
	return name;
}
const char *Gethostname()
{
	const char*hostname=getenv("HOSTNAME");
	if(hostname==NULL)return "None";
	return hostname;
}
const char *Getcwd()
{
	const char*pwd=getenv("PWD");
	if(pwd==NULL)return "None";
	return pwd;
}
void Makecommandline(char line[],size_t size)
{
	const char* username=Getusername();
	const char* hostname=Gethostname();
	const char*cwd=Getcwd();
	SkipPath(cwd);
	snprintf(line,size,"[%s@%s %s]> ",username,hostname,strlen(cwd)==1 ? "/":cwd+1);
	printf("%s",line);
	fflush(stdout);
}
int Getusercommand(char command[],size_t n)
{
	char *s=fgets(command,n,stdin);
	if(s==NULL)return -1;
	command[strlen(command)-1] = ZERO;
	return strlen(command);
}

void Splitcommand(char command[],size_t n)
{
	(void)n;
	gArgv[0]=strtok(command,SEP);
	int index=1;
	while(gArgv[index++]=strtok(NULL,SEP));
	//故意写成=，表示先赋值，再判断，分割之后，strtok会返回NULL,刚好让gARGV最后一个元素是NULL,并且while判断结束
}

    
void Executecommand()
{

	pid_t id=fork();
	if(id<0)
	{
		exit(1);
	}
	else if (id==0)
	{
		//
		if(redir_type==IN_Redir)
		{
			int fd=open(filename,O_RDONLY);

			dup2(fd,0);
		}
		else if(redir_type==OUT_Redir)
		{
			int fd=open(filename,O_WRONLY | O_CREAT | O_TRUNC);
			dup2(fd,1);
		}
		else if(redir_type==App_Redir)
		{
			int fd=open(filename,O_WRONLY | O_CREAT | O_APPEND);
			dup2(fd,1);
		}
		else
		{
		}
		//child
		execvp(gArgv[0],gArgv);
		exit(errno);
	}
	else
	{
		//father
		int status=0;
		pid_t rid=waitpid(id,&status,0);
		if(rid>0)
		{
			//wait sucess
			lastcode = WEXITSTATUS(status);
			if(lastcode!=0)printf("%s:%s:%d\n",gArgv[0],strerror(lastcode),lastcode);
		}
	}
}
const char *Gethome()
{
	const char *home=getenv("HOME");
	if(home==NULL)return "/root";
	return home;
}
void Cd()
{
	const char *path =gArgv[1];
	if(path==NULL)
	{
		path=Gethome();
	}
	//path一定存在
	chdir(path);
	//刷新
	char tmp[SIZE*2];
	getcwd(tmp,sizeof(tmp));
	snprintf(cwd,sizeof(cwd),"PWD=%s",tmp);
	putenv(cwd);

}

int Checkbuildin()
{
	int yes=0;
	const char * enter_cmd=gArgv[0];
	if(strcmp(enter_cmd,"cd")==0)
	{
		yes=1;
		Cd();
	}
	//else if(strcmp(enter_cmd,"echo")==0&& strcmp(gArgv[1],"$?")==0)
	//{
	//	yes=1;
	//	printf("%d\n",lastcode);
	//	lastcode = 0;
	//}
	return yes;
}

void CheckRedir(char usercommand[])
{
	int pos=0;
	int end=strlen(usercommand);
	while(pos<end)
	{
		if(usercommand[pos]=='>')
		{
			if(usercommand[pos+1]=='>')
			{
				usercommand[pos++]=0;
				pos++;
				redir_type = App_Redir;
				SkipSpace(usercommand,pos);
				filename=usercommand+pos;
			}
			else
			{
				usercommand[pos++]=0;
				redir_type = OUT_Redir;
				SkipSpace(usercommand,pos);
				filename=usercommand+pos;
			}
		}
		else if(usercommand[pos]=='<')
		{
			usercommand[pos++]=0;
			redir_type=IN_Redir;
			SkipSpace(usercommand,pos);
			filename =usercommand+pos;
		}
		else
		{
			pos++;
		}
	}
}

int main()
{
	int quit=0;
	while(!quit)
	{
		//0、重置
		redir_type = None_Redir;
		filename =NULL;
		//1、我们自己输入一个命令行
		char commandline[SIZE];
		Makecommandline(commandline,sizeof(commandline));

		//2、获取用户命令字符串分割
		char usercommand[SIZE];
		int n=Getusercommand(usercommand,sizeof(usercommand));
		if(n<=0)return 1;

		//2.1、checkredir
		CheckRedir(usercommand);


		//3、命令行字符串分割
		Splitcommand(usercommand,sizeof(usercommand));

		//int i;
		//intfor(i=0;gArgv[i];i++)
		//int{
		//int	printf("gArgv[%d]:%s\n",i,gArgv[i]);
		//int}

		//4、检测命令是否是内建命令
		n = Checkbuildin();
		if(n)continue;
		//5、执行命令
		Executecommand();
	}
	return 0;
}
