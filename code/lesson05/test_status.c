#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>

int main()
{
	pid_t id =fork();
	if(id==0)
	{
		//子进程
		int cnt=5;
		while(cnt)
		{
			printf("child:cnt:%d,pid:%d\n",cnt,getpid());
			sleep(1);
			cnt--;
		}
	}
	else
	{
		//父进程
		while(1)
		{
			printf("parent:running always! pid:%d\n",getpid());
			sleep(1);
		}
	}
	//while(1)
	//{
	//	sleep(1);
	//	printf("I am a process,pid:%d\n",getpid());
	//}
	return 0;
}
