#include<iostream>
#include<unistd.h>
#include<vector>
using namespace std;
const int num=10;
void func()
{
	while(1){
		cout<<"子进程：pid"<<getpid()<<" ,ppid:"<<getppid()<<endl;
		sleep(10);
	}
}
int main()
{
	vector<int> f;
	for(int i=0;i<num;i++)
	{
		pid_t id=fork();
		f.push_back(id);
		if(id==0){
				func();
			}
		}
	}
	for(auto e:f){
		cout<<e<<" ";
	}
	cout<<endl;
	sleep(10);
	return 0;
}
