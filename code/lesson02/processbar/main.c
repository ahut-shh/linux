#include<stdio.h>
#include<time.h>
#include<unistd.h>
#include<stdlib.h>
#include"process.h"

double total=2048.0; //2048MB
double once=0.1; 

void download()
{
	double current=0.0;
	while(current<total)
	{
		int r=rand()%20+1;//[1,20]
		double speed=r*once;
		current+=speed;
		if(current>=total)current=total;
		usleep(10000);

		process(total,current);


//		printf("test:%.1lf.%.1lf\r",current,total);
//		fflush(stdout);
	}
}
int main()
{
	srand(time(NULL));
	download();
//	process();
	return 0;
}
