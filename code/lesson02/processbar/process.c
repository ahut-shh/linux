#include"process.h"
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>

#define NUM 101
//进度条
//version2
void process(double total,double current){

	//更新当前进度的百分比
	double rate=(current/total)*100;
//	printf("test:%-.1lf %%\r",rate);
//	fflush(stdout);
	char bar[NUM];
	memset(bar,'\0',sizeof(bar));
	int i;
	for( i=0;i<(int)rate;i++){
		bar[i]='#';
	}
    
//  printf("%.1lf\r",rate);  
	printf("[%-s][%.1lf%%]\r",bar,rate);
	fflush(stdout);

}
//version 1
//void process()
//{
//	const char *label= "|/-\\";
//	char a[NUM];
//	memset(a,'\0',sizeof(a));
//	int cnt=0;
//	while(cnt<=100)
//	{
//		int len=cnt%4;
//		printf("[%-100s][%d %][%c]\r",a,cnt,label[len]);
//		a[cnt]='#';
//		cnt++;
//		fflush(stdout);
//		usleep(200000);
//	}
//	printf("\r\n");
//}
