#include<stdio.h>
#include<unistd.h>

int main()
{
	int count=10;
	while(count>=0){
		printf("%-2d\r",count);
		count--;
		fflush(stdout);
		sleep(1);
	}
	return 0;
}
